package ru.hennery.messages;

import java.io.Serializable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PostOffice {
	static final Logger logger = LogManager.getLogger(PostOffice.class.getName());
	
	private static DebugMessenger debugMessenger = null;
	private static EmailMessenger emailMessanger = null;
	
	private static <T extends Messenger<?>> T wakeUpMessenger(T messenger, Class<T> clazz) {
		
		if (messenger == null || !messenger.isAlive()) {
			try {
				messenger = clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException ignore) {				
				logger.catching(ignore);
			}
			messenger.start();
		}		 
		
		return messenger;
	}
	
	private static class MessengerQueueElement<T> {
			
		private T msg;
		private BiConsumer<T, Exception> then;
		
		public T getMsg() {
			return msg;
		}
		
		public BiConsumer<T, Exception> getThen() {
			return then;
		}		
		
		public MessengerQueueElement(T msg, BiConsumer<T, Exception> then) {
			this.msg = msg;
			this.then = then;
		}
		
	}
	
	abstract static class Messenger<T extends Serializable> extends Thread {
		
		protected static final int QUEUE_CAPACITY = 100;
		protected static final int WORKER_TIMEOUT = 10;
		
		protected ArrayBlockingQueue<MessengerQueueElement<T>> queue = new ArrayBlockingQueue<>(QUEUE_CAPACITY, true);
		
		public void putMsg(MessengerQueueElement<T> msg) {
			queue.add(msg);			
		}
		
		protected abstract void deliver(T msg) throws Exception;
		
		@Override
		public void run() {
			
			try {
				while(true) {
					MessengerQueueElement<T> element = queue.poll(WORKER_TIMEOUT, TimeUnit.SECONDS);
					if (element == null) return;
					
					
					Exception deliverException = null;
					try {
						deliver(element.getMsg());
					} catch (Exception e) {
						deliverException = e;
					}
					
					if (element.getThen() != null) element.getThen().accept(element.getMsg(), deliverException);
				}
				
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			
		}
		
	}
	
	public static void send (String msg, BiConsumer<String, Exception> then) {
		debugMessenger = (DebugMessenger) wakeUpMessenger(debugMessenger, DebugMessenger.class);
		debugMessenger.putMsg(new MessengerQueueElement<String>(msg, then));
	}
	
	public static void send(MsgEmail msg, BiConsumer<MsgEmail, Exception> then) {
		emailMessanger = (EmailMessenger) wakeUpMessenger(emailMessanger, EmailMessenger.class);
		emailMessanger.putMsg(new MessengerQueueElement<MsgEmail>(msg, then));			
	}
}

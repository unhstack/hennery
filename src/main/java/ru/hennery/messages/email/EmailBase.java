package ru.hennery.messages.email;

import java.util.Arrays;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;



class EmailBase implements Email {
	
	private String userName;
	private String password;
	private Properties properties;
	
	@Override
	public void send(String from, String[] to, String subj, String[] cc,
			String text) {
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
				
		Session session = Session.getInstance(properties,
				new Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(getUserName(), getPassword());
					}
				}
		);
		
		try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from));
            
            Arrays.asList(to);
            msg.addRecipients(Message.RecipientType.TO,  
            		Arrays.asList(to).stream().toArray(InternetAddress[] :: new)
            );            
            msg.addRecipients(Message.RecipientType.CC,  
            		Arrays.asList(cc).stream().toArray(InternetAddress[] :: new)
            );
            
            msg.setSubject(subj);
            msg.setText(text);            
            Transport.send(msg);
            
        } catch (AddressException e) {
            throw new RuntimeException(e);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }		
		
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	
}

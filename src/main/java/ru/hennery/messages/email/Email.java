package ru.hennery.messages.email;

public interface Email {
	default void send(String from, String to, String subj, String text) {
		send(from, new String[] {to}, subj, null, text);
	}
	void send(String from, String[] to, String subj, String[] cc, String text);	
	
}

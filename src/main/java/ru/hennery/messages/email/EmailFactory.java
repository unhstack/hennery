package ru.hennery.messages.email;

import java.util.Map;
import java.util.Properties;

import ru.hennery.utils.Options;

public class EmailFactory {
	private static enum SMTPConnection {TLS, SSL};
	
	private static EmailBase gMail(SMTPConnection smtpConnection) throws Exception {
		String grp = "GMAIL_SSL";
		if (smtpConnection.equals(SMTPConnection.TLS)) {
			grp = "GMAIL_TLS";
	    } 
		Map<String, String> mapProps = Options.getGroup(grp);
		
		String username = mapProps.get("username");
		String password = mapProps.get("password");
		
		mapProps.remove("username");
		mapProps.remove("password");
		
		EmailBase emailBase = new EmailBase();
		emailBase.setUserName(username);
		emailBase.setPassword(password);
		
		Properties props = new Properties();
		props.putAll(mapProps);
		
		return emailBase;
	}
	
	public static Email gMailSSL() throws Exception {
		return gMail(SMTPConnection.SSL);
	}
	
	public static Email gMailTLS() throws Exception {
		return gMail(SMTPConnection.TLS);
	}
}

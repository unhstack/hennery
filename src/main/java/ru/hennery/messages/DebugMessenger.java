package ru.hennery.messages;

import ru.hennery.messages.PostOffice.Messenger;

class DebugMessenger extends Messenger<String> {

	@Override
	protected void deliver(String msg) {
		
		PostOffice.logger.info("Debug sender: {}", msg);
		
	}
	
}
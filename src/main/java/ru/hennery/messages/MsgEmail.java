package ru.hennery.messages;

import java.io.Serializable;

public class MsgEmail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String from;
	private String to;
	private String subj;
	private String body;
	
	public MsgEmail(String from, String to, String subj, String body) {
		this.from = from;
		this.to = to;
		this.subj = subj;
		this.body = body;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubj() {
		return subj;
	}

	public void setSubj(String subj) {
		this.subj = subj;
	}

}

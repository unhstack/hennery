package ru.hennery.messages;

import ru.hennery.messages.PostOffice.Messenger;
import ru.hennery.messages.email.Email;
import ru.hennery.messages.email.EmailFactory;

class EmailMessenger extends Messenger<MsgEmail> {

	@Override
	protected void deliver(MsgEmail msg) throws Exception {
		
		EmailFactory.gMailSSL().send(msg.getFrom(), msg.getTo(), msg.getSubj(), msg.getBody());
		
	}
	
}
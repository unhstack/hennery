package ru.hennery.elements.switches;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.hennery.App;
import ru.hennery.db.DS;
import ru.hennery.elements.DBElement;

public class DBSwitch extends DBElement implements Switch {
	private static final Logger logger = LogManager.getLogger(DBElement.class.getName());
		
	private Boolean status = null;
	private Date lastStatusChange;
	
	@Override
	protected String getElementName() {
		return "switch";
	}

	@Override
	protected String createQuery() {
		return "insert into switch (id, name, driver_name, status, active) values (?, ?, ?, 0, 0)";
	}
	
	@Override
	protected String destroyQuery() {
		return "delete from switch where id = ?";
	}

	
	public DBSwitch() {
		
	}
	
	public DBSwitch(Long id ) {
		this.id = id; 
	}
	
	@Override
	public boolean getStatus() throws Exception {
		if (status == null) {
			try (DS ds = App.getDS()) {
				status = (Boolean) ds.sql("select status from switch where id = ?", id).get(0).get("STATUS");
			}
		}
		return status;
	}
	
	private void setStatus(boolean status) throws Exception {
		if (!getActive()) return;
		
		Date now = Calendar.getInstance().getTime();
		if (d == null) d = DriverFactory.getDriver(getDriverName());
		
		if (status) ((Driver) d).on(); else ((Driver) d).off();
		
		String sql = "update switch set status = ?, last_status_change = ? where id = ?";
		try (DS ds = App.getDS()) {
			ds.update(sql, status, now, id);
			ds.commit();
		}
		this.status = status;
		lastStatusChange = now;
	}

	@Override
	public Date getLastStatusChange() throws Exception {
		if (lastStatusChange == null) {
			try (DS ds = App.getDS()) {
				List<Map<String, Object>> res = ds.sql("select last_status_change from switch where id = ?", id);
				lastStatusChange = (Date) res.get(0).get("LAST_STATUS_CHANGE");
			}
		}
		return lastStatusChange;
	}

	@Override
	public void on() throws Exception {
		setStatus(true);
		logger.info("Switch name: {} is on!", name);

	}

	@Override
	public void off() throws Exception {
		setStatus(false);
		logger.info("Switch name: {} is off!", name);
	}

}

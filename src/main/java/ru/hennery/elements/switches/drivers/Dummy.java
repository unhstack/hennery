package ru.hennery.elements.switches.drivers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.hennery.elements.switches.Driver;


public class Dummy implements Driver {
	private static final Logger logger = LogManager.getLogger(Dummy.class.getName()); 
	public static final String name = "Dummy Switch";
	
	public Dummy() {
		
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void on() {
		logger.info("Switch ON");
	}

	@Override
	public void off() {
		logger.info("Switch OFF");
	}

}

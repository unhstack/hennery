package ru.hennery.elements.switches.drivers;

import ru.hennery.elements.switches.Driver;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

public class Relay implements Driver {
	public static final String NAME_PREF = "RelayGPIO_"; 
	public static final String name = NAME_PREF + "XX"; 

	private GpioPinDigitalOutput pin; 
	
	public Relay(Pin pin) 
	{
		GpioController con = GpioFactory.getInstance();
		if ((this.pin = (GpioPinDigitalOutput) con.getProvisionedPin(pin)) == null)
			this.pin = con.provisionDigitalOutputPin(pin, PinState.HIGH); //почемуто именно так оно выключено
		this.pin.setShutdownOptions(true, PinState.HIGH);
	}
	
	@Override
	public String getName() {
		return "Relay " + pin.getName();
	}
	
	@Override
	public void on() {
		 pin.low();
	}

	@Override
	public void off() {
		 pin.high();
	}

}

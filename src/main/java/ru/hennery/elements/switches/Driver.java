package ru.hennery.elements.switches;

import ru.hennery.elements.DriverBase;

public interface Driver extends DriverBase {
	public void on();
	public void off();
}

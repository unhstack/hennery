package ru.hennery.elements.switches;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

import ru.hennery.App;
import ru.hennery.elements.DriverFactoryBase;
import ru.hennery.elements.switches.Driver;
import ru.hennery.elements.switches.drivers.Relay;

public class DriverFactory extends DriverFactoryBase {
	
	private static final String DRIVER_PACKAGE_NAME = "ru.hennery.elements.switches.drivers";
	private static final Logger logger = LogManager.getLogger(DriverFactory.class.getName()); 
	
	public static List<DriverDesc> getDrivers() throws Exception {
		 List<DriverDesc> drivers = getDrivers(DRIVER_PACKAGE_NAME);
		 List<DriverDesc> result = new ArrayList<>();
		 for (DriverDesc it : drivers) {
			 if (it.className.equals(Relay.class.getSimpleName())) {
				 for (int i = 0; i <= 9; i ++ ) {
					 result.add(new DriverDesc(Relay.NAME_PREF + i, Relay.class.getSimpleName() + i));
				 }
				 continue;
			 }
			 result.add(it);
		 }
		 
		return result;
	}
	
	public static Driver getDriver(String className) throws Exception {
		if (className.startsWith(Relay.class.getSimpleName())) {
			String pinName = "GPIO " + className.substring(Relay.class.getSimpleName().length());
			Pin pin = RaspiPin.getPinByName(pinName);
			logger.info("pin = {} pinName = {}", pin , pinName);
			
			Constructor<?> ctor = Relay.class.getConstructor(Pin.class);
			return (Driver) ctor.newInstance(pin);
		}
		return (Driver) getDriver(className, DRIVER_PACKAGE_NAME);
	}

}

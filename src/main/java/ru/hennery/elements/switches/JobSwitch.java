package ru.hennery.elements.switches;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ru.hennery.App;

public class JobSwitch implements Job {
	private boolean swOn;
	
	public JobSwitch() {
		
	}
	
	public JobSwitch(boolean swOn) {
		this.swOn = swOn;
	}
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		try {
			Long switchId = context.getJobDetail().getJobDataMap().getLongValue("id");
			Switch sw = App.getSwitches().get(switchId);
			if (sw == null) throw new Exception("Switches get switchId " + switchId + " return null!");
			if (swOn) sw.on(); else sw.off();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}

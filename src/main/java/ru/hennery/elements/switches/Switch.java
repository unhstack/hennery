package ru.hennery.elements.switches;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import ru.hennery.elements.Element;


public interface Switch  extends Element {
	public static final String JOB_GROUP 	 = "Switches";
	public static final String JOB_NAME_ON  = "switch_on";
	public static final String JOB_NAME_OFF = "switch_off";
	
	@XmlElement
	boolean getStatus()  throws Exception;
	@XmlElement
	String getDriverName() throws Exception;
	void setDriverName(String driverName) throws Exception;
	@XmlElement
	Date getLastStatusChange() throws Exception;
	
	void on() throws Exception;
	void off() throws Exception;

	default void hardwareSync() throws Exception {
		if (getStatus()) on(); else off();
	}
}

package ru.hennery.elements;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public interface Element {
	static final String JOB_GROUP = "HenneryElementsGroup";
	static final String JOB_NAME  = "HenneryElement";
	
	@XmlElement
	Long getId();	
	@XmlElement
	boolean getActive() throws Exception;	
	void setActive(boolean value) throws Exception;	
	
	@XmlElement
	String getName() throws Exception;
	void setName(String name) throws Exception;
	
	Long create(String name, String driverName) throws Exception;
	void destroy() throws Exception;
}

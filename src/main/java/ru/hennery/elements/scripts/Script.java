package ru.hennery.elements.scripts;

import java.util.Date;
import java.util.Map;














import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import ru.hennery.elements.Element;
import ru.hennery.elements.sensors.Sensor;
import ru.hennery.elements.switches.Switch;
@XmlRootElement
public interface Script  extends Element {

	
	@XmlTransient
	Map<String, Sensor> getSensors() throws Exception;
	void addSensor(Sensor sensor, String scriptName) throws Exception;
	void removeSensor(Sensor s) throws Exception;
	
	@XmlTransient
	Map<String, Switch> getSwitches() throws Exception;	
	void addSwitch(Switch sw, String scriptName) throws Exception;
	void removeSwitch(Switch sw) throws Exception;
	
	@XmlElement
	String getScriptBody()  throws Exception;
	void setScriptBody(String scriptBody)  throws Exception;
	
	@XmlElement
	Date getLastRunningTime() throws Exception;
	
	Long create(String name) throws Exception;
	
	void run() throws Exception;
	
}

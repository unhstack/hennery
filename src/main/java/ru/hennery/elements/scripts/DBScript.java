package ru.hennery.elements.scripts;

import java.sql.Clob;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.SimpleScriptContext;

import ru.hennery.App;
import ru.hennery.db.DS;
import ru.hennery.elements.DBElement;
import ru.hennery.elements.sensors.Sensor;
import ru.hennery.elements.switches.Switch;

public class DBScript extends DBElement implements Script{
	private Map<String, Sensor> sensors = null;
	private Map<String, Switch> switches = null;
	private Date lastRunningTime;
	
	@Override
	protected String getElementName() {
		return "script";
	}

	@Override
	protected String getSelectDriverNameQuery() {
		return null;
	}

	@Override
	protected String getUpdateDriverNameQuery() {
		return null;
	}

	@Override
	protected String createQuery() {
		return null;
	}

	@Override
	protected String destroyQuery() {
		return null;
	}

	
	public DBScript () {
		
	}
	
	public DBScript(Long id) {
		this.id = id;		
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public Map<String, Sensor> getSensors() throws Exception {
		if (sensors == null) {
			try (DS ds = App.getDS()) {
				sensors =  ds.sql("select id_sensor, script_name from sensor_script where id_script = ?", id).stream()
					.collect(
							Collectors.toMap(
									it -> (String) it.get("SCRIPT_NAME"),
									it -> App.getSensors().get(it.get("ID_SENSOR"))
								)
				);
	
			}
		}
		return sensors;
	}
	
	@Override
	public void addSensor(Sensor sensor, String scriptName) throws Exception {
		if (sensors == null) getSensors();
		try (DS ds = App.getDS()) {
			String sql = "insert into sensor_script (id_sensor, id_script, script_name) values (?, ?, ?)";
			ds.update(sql, sensor.getId(), id, scriptName);
			ds.commit();
		}
		sensors.put(scriptName, sensor);
	}
	
	@Override
	public void removeSensor(Sensor sensor) throws Exception {
		if (sensors == null) getSensors();
		try (DS ds = App.getDS()) {
			String scriptName = ds.getString("select script_name from sensor_script where id_sensor = ? and id_script = ?", sensor.getId(), id);
			if (scriptName == null ) throw new Exception("Not found sensor_script  id_sensor = " + sensor.getId() + " and id_script = " + id);
			ds.update("delete from sensor_script where id_sensor = ? and id_script = ?", sensor.getId(), id);
			sensors.remove(scriptName);
			ds.commit();
		}
		
	}	
	
	@Override
	public Map<String, Switch> getSwitches() throws Exception {
		if (switches == null) {
			try (DS ds = App.getDS()) {
				String sql = "select id_switch, script_name from switch_script where id_script = ?";
				switches = ds.sql(sql, id).stream()
						.collect(
								Collectors.toMap(
										it -> (String) it.get("SCRIPT_NAME"),
										it -> App.getSwitches().get((Long) it.get("ID_SWITCH"))
									)
					);
	
			}
		}
		return switches;
	}
	
	@Override
	public void addSwitch(Switch sw, String scriptName) throws Exception {
		if (switches == null) getSwitches();
		try (DS ds = App.getDS()) {
			String sql = "insert into switch_script (id_switch, id_script, script_name) values (?, ?, ?)";
			ds.update(sql, sw.getId(), id, scriptName);
			ds.commit();
		}
		switches.put(scriptName, sw);
	}
	
	@Override
	public void removeSwitch(Switch sw) throws Exception {
		if (switches == null) getSwitches();
		try (DS ds = App.getDS()) {
			String sql = "select script_name from switch_script where id_switch = ? and id_script = ?";
			String scriptName = ds.getString(sql, sw.getId(), id);
			if (scriptName == null) throw new Exception("Not found switch_script where id_switch = " + sw.getId() + " and id_script = " + id);
			sql = "delete from switch_script where id_switch = ? and id_script = ?";
			ds.update(sql, sw.getId(), id);
			switches.remove(scriptName);
			ds.commit();
		}
		
	}
	
	@Override
	public String getScriptBody() throws Exception {
		try (DS ds = App.getDS()) {
			String sql = "select script_body from script where id = ?";
			List<Map<String, Object>> res = ds.sql(sql, id);
			Clob cScript = (Clob) res.get(0).get("SCRIPT_BODY");
			if (cScript == null) return null;
			return cScript.getSubString(1, (int) cScript.length());
		}
	}

	@Override
	public void setScriptBody(String scriptBody) throws Exception {
		try (DS ds = App.getDS()) {
			String sql = "update script set script_body = ? where id = ?";
			ds.update(sql, scriptBody, id);
			ds.commit();
		}
	}
	
	@Override
	public Date getLastRunningTime() throws Exception {
		if (lastRunningTime == null) {
			try (DS ds = App.getDS()) {
				lastRunningTime = (Date) ds.sql("select last_running_time from script where id = ?", id).get(0).get("LAST_RUNNING_TIME");
			}
		}
		return lastRunningTime;
	}
	
	@Override
	public Long create(String name, String driverName) throws Exception {
		throw new Exception("Not Implemented");
	}
	@Override
	public Long create(String name) throws Exception {
		try (DS ds = App.getDS()) {
			id = ds.generateId("script");
			String sql = "insert into script "
					+ "(id, name, active, script_body)"
					+ " values "
					+ "(?, ?, ?, ?)";
			ds.update(sql, id, name, false, null);
			ds.commit();
		}
		this.name = name;
		active =  false;		
		return id;
	}

	@Override
	public void destroy() throws Exception {
		try (DS ds = App.getDS()) {
			String sql = "delete from switch_script where id_script = ?";
			ds.update(sql, id);
			sql = "delete from sensor_script where id_script = ?";
			ds.update(sql, id);
			sql = "delete from script where id = ?";
			ds.update(sql, id);
			ds.commit();
		}
	}

	@Override
	public void run() throws Exception {
		if (!getActive()) return;
		
		Date now = Calendar.getInstance().getTime();
		String script = getScriptBody();
		
	    ScriptEngine engine = App.getScriptEngine();
	    ScriptContext context = new SimpleScriptContext();	    
	    Bindings engineScope = context.getBindings(ScriptContext.ENGINE_SCOPE);
	    engineScope.putAll(getSensors());
	    engineScope.putAll(getSwitches());

	    engine.eval(script, context);
	    
	    try (DS ds = App.getDS()) {
	    	ds.update("update script set last_running_time = ? where id = ?", now, id);
	    	ds.commit();
	    }
	    lastRunningTime = now;
	}


	
}

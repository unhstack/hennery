package ru.hennery.elements.scripts;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ru.hennery.App;

public class JobScript implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			Long scriptId = context.getJobDetail().getJobDataMap().getLongValue("id");
			Script script = App.getScripts().get(scriptId);			
			script.run();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}

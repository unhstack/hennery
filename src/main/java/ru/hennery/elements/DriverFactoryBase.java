package ru.hennery.elements;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.hennery.utils.ClassUtils;

public class DriverFactoryBase {
	@XmlRootElement
	public static class DriverDesc {
		
		@XmlElement
		public String name;
		@XmlElement
		public String className;
		
		
		public DriverDesc() {
			
		}
		
		public DriverDesc(String name, String className) {
			this.name = name;
			this.className = className;
		}
	}
	
	private static String driverClassFullName(String className, String driverPackageName) {
		return driverPackageName + "." + className;
	}
	
	private static String driverName(String className, String driverPackageName) {
		String name = null;
		try {
			name = (String) Class.forName(driverClassFullName(className, driverPackageName)).getDeclaredField("name").get(null);
		} catch (Exception e) {
		}
		return name;
	}	
	
	protected static List<DriverDesc> getDrivers(String driverPackageName) throws Exception {
		
		List<String> res = ClassUtils.getCachedClassesNamesInPackage(driverPackageName);
		return res.stream()
			.map((it)-> (
					new DriverDesc(driverName(it, driverPackageName), it)) 
					 )
			.collect(Collectors.toList()
			);
	}
	
	protected static Object getDriver(String driverClassName, String driverPackageName) throws Exception {
		
		Class<?> clazz = Class.forName(driverClassFullName(driverClassName, driverPackageName));
		Constructor<?> ctor = clazz.getConstructor();
		return ctor.newInstance();
	}

}

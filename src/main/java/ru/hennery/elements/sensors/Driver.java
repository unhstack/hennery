package ru.hennery.elements.sensors;

import java.math.BigDecimal;

import ru.hennery.elements.DriverBase;

public interface Driver extends DriverBase {
	public BigDecimal getValue() throws Exception;
	public String getUnit();
}

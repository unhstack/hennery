package ru.hennery.elements.sensors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ru.hennery.App;

public class JobSensor implements Job {
	

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		try {			
			Long sensorId = context.getJobDetail().getJobDataMap().getLongValue("id");
			Sensor sensor = App.getSensors().get(sensorId);
			sensor.read();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}
	}

}

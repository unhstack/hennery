package ru.hennery.elements.sensors;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.hennery.elements.Element;
/*
 * Сенсор любой. 
 * Предоставляет название
 * Последнее значение (значение , дату и единицу измерений)
 */
@XmlRootElement
public interface Sensor extends Element {
	public static final String JOB_GROUP = "Sensors";
	public static final String JOB_NAME  = "sensor";
	
	@XmlElement
	Reading getLastReading() throws Exception;
	
	Reading read() throws Exception;
		
	List<Reading> getReadings() throws Exception;
	@XmlElement
	String getDriverName() throws Exception;
	void setDriverName(String driverName) throws Exception;
	
}

package ru.hennery.elements.sensors.drivers;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.hennery.elements.sensors.Driver;

public class Dummy implements Driver {
	static final Logger logger = LogManager.getLogger(Dummy.class.getName()); 
	public static final String name = "Dummy Sensor";
	
	private double x = 0; 
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public BigDecimal getValue() {
		double y = Math.sin(x > 360 ? (x = 0) : x++);	
		logger.info("value = {}", y);
		return new BigDecimal(y);
	}

	@Override
	public String getUnit() {
		return "пк";
	}

}

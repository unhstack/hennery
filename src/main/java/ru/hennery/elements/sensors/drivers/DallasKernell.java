package ru.hennery.elements.sensors.drivers;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import ru.hennery.elements.sensors.Driver;
import ru.hennery.utils.Options;

public class DallasKernell implements Driver {

	public static final String PATH = Options.getAsString(null, "dallas_path", "/sys/bus/w1/devices"); 
	public static final String NAME_PREF = "DALLAS-";
	private static final String FN = "w1_slave";
	
	private String id;  
	
	public DallasKernell(String id) {
		this.id = id;
	}
	
	@Override
	public String getName() {
		return NAME_PREF + id;
	}

	@Override
	public BigDecimal getValue() throws Exception {
		Long result = null; 
				
		List<String> lines = Files.readAllLines(Paths.get(PATH + File.separator + id, FN));
		
		if (lines.size() > 0 && lines.get(0).contains("YES")  ) {
			result = new Long(lines.get(1).split("t=")[1]);
		}
		
		return result == null ? null : new BigDecimal(result / 1000);
	}

	@Override
	public String getUnit() {
		return "C";
	}

}

package ru.hennery.elements.sensors.drivers;

import java.math.BigDecimal;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;

import ru.hennery.elements.sensors.Driver;

public class DHT2211 implements Driver {
	
	// This is the only processor specific magic value, the maximum amount of time to
	// spin in a loop before bailing out and considering the read a timeout.  This should
	// be a high value, but if you're running on a much faster platform than a Raspberry
	// Pi or Beaglebone Black then it might need to be increased.
	private static final int DHT_MAXCOUNT = 32000;
	
	// Number of bit pulses to expect from the DHT.  Note that this is 41 because
	// the first pulse is a constant 50 microsecond pulse, with 40 pulses to represent
	// the data afterwards.
	private static final int DHT_PULSES = 41;
		
	
	private Pin pin;
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getValue() throws Exception {
		int temperature = 0;
		int humidity = 0;
		
		int[] pulseCounts = new int[DHT_PULSES * 2];
		
		GpioPinDigitalOutput pinOut =  GpioFactory.getInstance().provisionDigitalOutputPin(pin);
		
		//критичная по времени часть. заработает?
		pinOut.high();
		Thread.sleep(500);
		pinOut.low();		
		Thread.sleep(20);
		
		GpioPinDigitalInput pinIn = GpioFactory.getInstance().provisionDigitalInputPin(pin);
		Thread.sleep(1);
		int count = 0;
		while(pinIn.isHigh()) {
			if (++count >= DHT_MAXCOUNT) {
				throw new Exception("Timeout waiting for responses");
			}
		}
		for (int i=0; i < DHT_PULSES * 2; i +=2) {
			while (pinIn.isLow()) {
				if (++ pulseCounts[i] >= DHT_MAXCOUNT) {
					throw new Exception("Timeout waiting for responses");	
				}
			}
			while (pinIn.isHigh()) {
				if (++ pulseCounts[i+1] >= DHT_MAXCOUNT) {
					throw new Exception("Timeout waiting for responses");
				}
			}
		}
		// Compute the average low pulse width to use as a 50 microsecond reference threshold.
		  // Ignore the first two readings because they are a constant 80 microsecond pulse.
		  int threshold = 0;
		  for (int i=2; i < DHT_PULSES*2; i+=2) {
		    threshold += pulseCounts[i];
		  }
		  threshold /= DHT_PULSES-1;
		
		  byte[] data = new byte[5];
		  for (int i=3; i < DHT_PULSES*2; i+=2) {
		    int index = (i-3)/16;
		    data[index] <<= 1;
		    if (pulseCounts[i] >= threshold) {
		      // One bit for long pulse.
		      data[index] |= 1;
		    }
		    // Else zero bit for short pulse.
		  }

		  
		return null;
	}

	@Override
	public String getUnit() {
		// TODO Auto-generated method stub
		return null;
	}

	public DHT2211(Pin pin) {
		this.pin = pin;
	}

}

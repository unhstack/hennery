package ru.hennery.elements.sensors;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Reading {
	
	private BigDecimal value;
	private String unit;
	private Date dt;
	
	public Reading() {
		
	}
	
	public Reading(BigDecimal value, String unit, Date dt) { 
		this.value = value;
		this.unit = unit;
		this.dt = dt;
	}
	
	@XmlElement
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	@XmlElement
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	@XmlElement
	public Date getDt() {
		return dt;
	}
	public void setDt(Date dt) {
		this.dt = dt;
	}
	
}

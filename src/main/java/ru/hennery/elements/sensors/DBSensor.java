package ru.hennery.elements.sensors;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.hennery.App;
import ru.hennery.db.DS;
import ru.hennery.elements.DBElement;


public class DBSensor extends DBElement implements Sensor {
	
	private static final Logger logger = LogManager.getLogger(DBSensor.class.getName());
	
	private Reading lastReading;	
	
	@Override
	protected String getElementName() {
		return "sensor";
	}
	
	@Override
	protected String createQuery() {
		return "insert into sensor (id, name, last_value, last_unit, last_dt, active, driver_name) "
				+ "values (?, ?, null, null, null, 0, ?)";
	}

	@Override
	protected String destroyQuery() {
		return "delete from sensor where id = ?";
	}

	
	private void purgeReadig() throws Exception {
		
		try (DS ds = App.getDS()) {
			String sql = "select storage_count, storage_mils from sensor where id = ?";
			List<Map<String, Object>> sqlRes = ds.sql(sql, id);
			int storCount = (int) sqlRes.get(0).get("STORAGE_COUNT");
			long storMils = sqlRes.get(0).get("STORAGE_MILS") == null ? -1 : (long) sqlRes.get(0).get("STORAGE_MINITS");
			
			if (storMils == -1) {
				sql = "delete from reading where id in (select id from reading where id_sensor = ? order by dt desc limit ?)";
				logger.debug("sensor: {} purged by mils deleted: {}", name, 
						ds.update(sql, id, storCount)
				);
			} else {
				long curDt = Calendar.getInstance().getTimeInMillis();
				Date dt = new Date(curDt + storMils);
				sql = "delete from reading where id_sensor = ? and dt > ?";
				logger.debug("sensor: {} purged by count deleted: {}", name,				
						ds.update(sql, id, dt)
				);
			}
			ds.commit();
		}
	}


	@Override
	public Reading getLastReading() throws Exception {
		if (lastReading == null) {
			try (DS ds = App.getDS()) {
				String sql = "select dt, value, unit from reading where id_sensor = ? order by dt desc limit 1";
				List<Map<String, Object>> res = ds.sql(sql, this.id);
				if (res.size() == 0) return null;
				Map<String, Object> it = res.get(0); 
				lastReading = new Reading((BigDecimal) it.get("VALUE"), (String) it.get("UNIT"), (Date) it.get("DT"));
			}
		}
		return lastReading;
	}

	@Override
	public List<Reading> getReadings() throws Exception {
		try (DS ds = App.getDS()) {
			String sql = "select value, unit, dt from reading where id_sensor = ? order by dt";
			
			return ds.sql(sql, id).stream()
				.map(it -> new Reading((BigDecimal)it.get("VALUE"), (String)it.get("UNIT"), (Date) it.get("DT")))
				.collect(Collectors.toList());
		}
	}

	@Override
	public Reading read() throws Exception {
		if (!getActive()) return null;
		if (d == null) d = DriverFactory.getDriver(getDriverName());
		Driver sd = (Driver) d;
		
		purgeReadig();
		
		BigDecimal value = sd.getValue();
		String unit = sd.getUnit();
		
		try (DS ds = App.getDS()) {
			Date dt = Calendar.getInstance().getTime();
			String sql = "update sensor set last_value = ?, last_unit = ?, last_dt = ? where id = ?";
			ds.update(sql, value, unit, dt, id);
			long r_id = ds.generateId("reading");
			sql = "insert into reading (id, dt, id_sensor, value, unit) values (?,?,?,?,?)";
			ds.update(sql, r_id, dt, id, value, unit);
			ds.commit();
			return (lastReading = new Reading(value, unit, dt));
		}
	}
	
	public DBSensor(long id) {
		this.id = id;
	}

	public DBSensor() {		
	}

}

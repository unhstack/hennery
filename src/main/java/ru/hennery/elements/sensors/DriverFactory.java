package ru.hennery.elements.sensors;

import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import ru.hennery.elements.DriverFactoryBase;
import ru.hennery.elements.sensors.drivers.DallasKernell;

public class DriverFactory extends DriverFactoryBase  {
	
	private static final String DRIVER_PACKAGE_NAME = "ru.hennery.elements.sensors.drivers";
		
	public static List<DriverDesc> getDrivers() throws Exception {
		List<DriverDesc> drivers = getDrivers(DRIVER_PACKAGE_NAME);
		final List<DriverDesc> result = new ArrayList<DriverDesc>();
		for (DriverDesc it : drivers) {
			if (it.className.equals(DallasKernell.class.getSimpleName())) {
				Files.walk(Paths.get(DallasKernell.PATH))
					.map(p -> p.getFileName().toString())
					.filter(s -> s.matches("\\d\\d-\\w{12}"))
					.forEach(s->result.add(new DriverDesc(DallasKernell.NAME_PREF + s, DallasKernell.class.getSimpleName() + s)) );
			}
		}
		return result;
	}
	
	public static Driver getDriver(String className) throws Exception {
		if (className.startsWith(DallasKernell.class.getSimpleName())) {
			Constructor<?> ctor = DallasKernell.class.getConstructor(String.class);
			return (Driver) ctor.newInstance(className.substring(DallasKernell.class.getSimpleName().length()));
		}
		return (Driver) DriverFactoryBase.getDriver(className, DRIVER_PACKAGE_NAME);
	}
}

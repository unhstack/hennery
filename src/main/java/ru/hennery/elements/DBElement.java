package ru.hennery.elements;

import java.util.List;
import java.util.Map;

import ru.hennery.App;
import ru.hennery.db.DS;

public abstract class DBElement implements Element {
	
	protected Long id;
	protected Boolean active;
	protected String name;
	protected String driverName;
	protected DriverBase d;
	
	protected abstract String getElementName();
	protected String getSelectActiveQuery() {
		return "select active from " + getElementName() + " where id = ?";
	}
	protected String getUpdateActiveQuery() {
		return "update " + getElementName() + " set active = ? where id = ?";
	}
	protected String getSelectNameQuery() {
		return "select name from " + getElementName() + " where id = ?";
	}
	protected String getUpdateNameQuery() {
		return "update " + getElementName() + " set name = ? where id = ?";
	}
	protected String getSelectDriverNameQuery() {
		return "select driver_name from " + getElementName() + " where id = ?";
	}
	protected String getUpdateDriverNameQuery() {
		return "update " + getElementName() + " set driver_name = ? where id = ?";
	}
	protected abstract String createQuery();
	protected abstract String destroyQuery();
	
	
	public DBElement() {
		this.id = null;
	}
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public boolean getActive() throws Exception {
		if (active == null) {
			try (DS ds = App.getDS()) {
				active = (Boolean) ds.sql(getSelectActiveQuery(), id).get(0).get("ACTIVE");
			}
		}
		return active;
	}

	@Override
	public void setActive(boolean value) throws Exception {
		try (DS ds = App.getDS()) {
			String sql = getUpdateActiveQuery();
			ds.update(sql, value, id);	
			ds.commit();
		}
		active = value;		
	}

	@Override
	public String getName() throws Exception {
		if (name == null) {
			try (DS ds = App.getDS()) {
				name = (String) ds.sql(getSelectNameQuery() , id).get(0).get("NAME");
			}
		}
		return name;		
	}

	@Override
	public void setName(String name) throws Exception {
		try(DS ds = App.getDS()) {
			String sql = getUpdateNameQuery();
			ds.update(sql, name, id);
			ds.commit();
		}
		this.name = name;		
	}

	public String getDriverName() throws Exception {
		if (driverName == null) {
			try (DS ds = App.getDS()) {
				List<Map<String, Object>> res = ds.sql(getSelectDriverNameQuery(), id);
				driverName = (String) res.get(0).get("DRIVER_NAME");
			}
		}
		return driverName;
	}

	public void setDriverName(String driverName) throws Exception {
		try (DS ds = App.getDS()) {
			ds.update(getUpdateDriverNameQuery(), driverName, id);
			d = null;
			ds.commit();
		}
		this.driverName = driverName;
	}
		
	@Override
	public Long create(String name, String driverName) throws Exception {
		try(DS ds = App.getDS()) {
			id = ds.generateId(getElementName());
			
			String sql = createQuery();
			
			ds.update(sql, id, name, driverName);
			ds.commit();
		}
		this.name = name;
		this.driverName = driverName;
		this.active = false;
		return id;
	}

	@Override
	public void destroy() throws Exception {
		try (DS ds = App.getDS()) {
			String sql = destroyQuery();
			ds.update( sql, id);
			ds.commit();
		}
	}

}

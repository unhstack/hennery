package ru.hennery.elements.alerts.drivers;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.hennery.elements.alerts.Alert;
import ru.hennery.elements.alerts.Driver;
import ru.hennery.messages.PostOffice;

public class DebugSender implements Driver {
	private static final Logger logger = LogManager.getLogger(DebugSender.class.getName());
	
	private static final String name = "Debug sender";
	
	public DebugSender() {
		
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void send(Alert alert) throws Exception {
		try {

			PostOffice.send(alert.getMsg(),
					(m, ex) -> {
						try {
							alert.setStatus(Alert.Status.DELIVERED);
						} catch (Exception e) {
							logger.catching(e);
						}
					}
			);
		} catch (Exception e) {
			logger.catching(e);
		}
	}

}

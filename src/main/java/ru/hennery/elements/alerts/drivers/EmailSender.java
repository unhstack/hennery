package ru.hennery.elements.alerts.drivers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.hennery.elements.alerts.Alert;
import ru.hennery.elements.alerts.Driver;
import ru.hennery.messages.MsgEmail;
import ru.hennery.messages.PostOffice;

public class EmailSender implements Driver {
	private static final Logger logger = LogManager.getLogger(EmailSender.class.getName());
	
	private static final String name = "Debug sender";
	private static final String MSG_TO_PARAM_EMPTY = "Параметр to пустой!";
	private static final String MSG_BODY_EMPTY = "Тело сообщения пустое!";
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void send(Alert alert) throws Exception {
		if (alert.getParam("to") == null) throw new Exception(MSG_TO_PARAM_EMPTY);
		if (alert.getMsg() == null) throw new Exception(MSG_BODY_EMPTY);
		MsgEmail msg = new MsgEmail(
				alert.getParam("from"),
				alert.getParam("subj"),
				alert.getParam("to"), 
				alert.getMsg()
				);
		PostOffice.send(msg,
				(m, e) -> {
					if (e == null) {
						alert.setStatus(Alert.Status.DELIVERED);
					} else {
						alert.setStatus(Alert.Status.ERROR);
						alert.setErr(e.getMessage());
					}
				}
		);
		
	}

}

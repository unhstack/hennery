package ru.hennery.elements.alerts;

import java.util.Date;
import java.util.List;
import java.util.Map;

import ru.hennery.elements.Element;
import ru.hennery.services.results.JsonLabelValue;

public interface Alert extends Element {
	
	static enum Status {WAITING, DELIVERED, ERROR};
	
	void alert() throws Exception;
	
	void setMsg(String msg) throws Exception;
	String getMsg() throws Exception;

	Map<String, Driver> listSenders() throws Exception;
	void addSender(Driver sender) throws Exception;
	void removeSender(Driver sender) throws Exception;

	Map<String, String> getParams() throws Exception;
	void setParam(String name, String value) throws Exception;
	String getParam(String name) throws Exception;
	
	void setStatus(Status status);
	Status getStatus() throws Exception;
	void setErr(String err);
	String getErr() throws Exception;
	
	Date getStatusTime() throws Exception;
	
	Long create(String name, String msg) throws Exception;
	
}

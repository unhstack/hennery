package ru.hennery.elements.alerts;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import ru.hennery.App;

public class JobAlert implements Job {

	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		try {			
			Long alertId = context.getJobDetail().getJobDataMap().getLongValue("id");
			Alert sensor = App.getAlerts().get(alertId);
			sensor.alert();
		} catch (Exception e) {
			throw new JobExecutionException(e);
		}

	}

}

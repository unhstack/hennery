package ru.hennery.elements.alerts;


import java.sql.Clob;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.hennery.App;
import ru.hennery.db.DS;
import ru.hennery.elements.DBElement;
import ru.hennery.elements.alerts.drivers.EmailSender;
import ru.hennery.services.results.JsonLabelValue;

public class DBAlert extends DBElement implements Alert {
	private static final Logger logger = LogManager.getLogger(DBAlert.class.getName());
	
	private Map<String, Driver> senders = null;
	private String msg;
	private Status status;
	private String err;
	private Date statusTime;
	
	private Map<String, String> params = null;
	
	public DBAlert() {
	}
	
	public DBAlert(Long id) {
		this.id = id;
	}
	
	@Override
	protected String getElementName() {
		return "alert";
	}

	@Override
	protected String destroyQuery() {
		return "delete " + getElementName() + " where id = ?";
	}

	@Override
	protected String createQuery() {
		return null;
	}
	
	@Override
	public void alert() throws Exception {
		if (!getActive()) return;
		
		for(Driver sender : senders.values()) {
			sender.send(this);
		}
	}

	@Override
	public void setMsg(String msg) throws Exception {
		String sql = "update " + getElementName() + " set msg = ? where id = ?";
		try (DS ds = App.getDS()) {
			ds.update(sql, msg, id);
			ds.commit();
		}
		this.msg = msg;
	}

	@Override
	public String getMsg() throws Exception {
		if (msg == null) {
			String sql = "select msg from " + getElementName() + " where id = ?";
			try (DS ds = App.getDS()) {
				List<Map<String, Object>> res = ds.sql(sql, id);
				Clob cMsg = (Clob) res.get(0).get("MSG");
				msg = cMsg.getSubString(1, (int) cMsg.length());
			}
		}
		return msg;	
	}

	
	@Override
	public Map<String, Driver> listSenders() throws Exception {
		if (senders == null) {
			try (DS ds = App.getDS()) {
				senders = new HashMap<>();
				List<Map<String, Object>> res = ds.sql("select sender_name from alert_senders where id_alert = ?", id);
				for(Map<String, Object> it : res ){
					String senderName = (String) it.get("SENDER_NAME");
					senders.put(senderName, DriverFactory.getDriver(senderName));
				}				
			}
		}
		return senders;
	}

	@Override
	public void addSender(Driver sender) throws Exception {
		if (senders == null) listSenders();
		try (DS ds = App.getDS()) {
			String sql = "insert into alert_senders (id_alert, sender_name) values (?, ?)";
			ds.update(sql, id, sender.getClass().getSimpleName() );
			ds.commit();
		}
		senders.put(sender.getClass().getSimpleName(), sender);
	}

	@Override
	public void removeSender(Driver sender) throws Exception {
		if (senders == null) listSenders();
		try (DS ds = App.getDS()) {
			String sql = "select script_name from alert_senders where id_alert = ? and sender_name = ? and script_name = ?";
			List<Map<String, Object>> res = ds.sql(sql, id, sender.getName());
			if (res.size() == 0 ) throw new Exception("Not found alert_senders  id_alert = " + id + " and sender_name = " + sender.getName());
			sql = "delete alert_senders where id_alert = ? and sender_name = ?";
			ds.update(sql, id, sender.getName());
			ds.commit();
			senders.remove(res.get(0).get("SCRIPT_NAME"));
		}		
		
	}
	
	public Map<String, String> getParamsMap() throws Exception {
		if (params == null) {
			try (DS ds = App.getDS()) {
				params = ds.sql("select name, value from param_alert where id_alert = ?", id).stream()
						.collect(
								Collectors.toMap(
										it -> (String) it.get("NAME"), 
										it -> (String) it.get("VALUE")
										)
							);
				
			}
		}
		return params;
	}

	@Override
	public Map<String, String> getParams() throws Exception {
		return getParamsMap();
		/*
		return getParamsMap().entrySet().stream()
				.map(en -> new JsonLabelValue(en.getKey(), en.getValue()))
				.collect(Collectors.toList());
				*/
	}

	@Override
	public void setParam(String name, String value) throws Exception {
		if (params == null) getParams();
		
		try (DS ds = App.getDS()) {
			if (params.containsKey(name)) {
				ds.update("update param_alert set value = ? where id_alert = ? and name = ?", value, id, name);
			} else {
				ds.update("insert into param_alert (id_alert, name, value) values (?, ?, ?)", id, name, value);
			}
			ds.commit();
			params.put(name, value);
		}
	}

	@Override
	public String getParam(String name) throws Exception {
		if (params == null) params = getParamsMap();
		return params.get(name);
	}
	
	@Override
	public void setStatus(Status status) {
		try {
			String sql = "update " + getElementName() + " set status = ? where id = ?";
			try (DS ds = App.getDS()) {
				ds.update(sql, status.toString(), id);
				ds.commit();
			}
			this.status = status;
		} catch (Exception e) {
			logger.catching(e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Status getStatus() throws Exception {
		if (status == null) {
			String sql = "select status from " + getElementName() + " where id = ?";
			try (DS ds = App.getDS()) {
				List<Map<String, Object>> res = ds.sql(sql, id);
				status = Status.valueOf((String) res.get(0).get("STATUS"));
			}
		}
		return status;
	}

	@Override
	public void setErr(String err) {
		try {
			String sql = "update " + getElementName() + " set err = ? where id = ?";
			try (DS ds = App.getDS()) {
				ds.update(sql, err, id);
				ds.commit();
			}
			this.err = err;
		} catch (Exception e) {
			logger.catching(e);
			throw new RuntimeException(e);
		}
	}
	

	@Override
	public String getErr() throws Exception {
		if (err == null) {
			String sql = "select err from " + getElementName() + " where id = ?";
			try (DS ds = App.getDS()) {
				List<Map<String, Object>> res = ds.sql(sql, id);
				statusTime = (Date) res.get(0).get("STATUS_TIME");
			}
		}
		return err;
	}
	
	@Override
	public Date getStatusTime() throws Exception {
		if (statusTime == null) {
			String sql = "select status_time from " + getElementName() + " where id = ?";
			try (DS ds = App.getDS()) {
				List<Map<String, Object>> res = ds.sql(sql, id);
				statusTime = (Date) res.get(0).get("STATUS_TIME");
			}
		}
		return statusTime;
	}

	@Override
	public Long create(String name, String msg ) throws Exception {
		try (DS ds = App.getDS()) {
			id = ds.generateId(getElementName());
			String sql = "insert into " + getElementName() 
					+ " (id, name, active, status, status_time, msg)"
					+ " values "
					+ "(?, ?, ?, ?, now(), ?)";
			ds.update(sql, id, name, 0, Status.WAITING.toString(), msg);
			ds.commit();
		}
		
		this.name = name;
		active =  false;
		this.msg = msg;
		
		return id;
	}

}

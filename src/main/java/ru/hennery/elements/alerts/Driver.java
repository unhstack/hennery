package ru.hennery.elements.alerts;

import ru.hennery.elements.DriverBase;

public interface Driver extends DriverBase {
	void send(Alert alert) throws Exception;
}

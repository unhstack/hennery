package ru.hennery.elements.alerts;

import java.util.List;

import ru.hennery.elements.DriverFactoryBase;
import ru.hennery.elements.alerts.Driver;

public class DriverFactory extends DriverFactoryBase {
	
	private static final String SENDER_PACKAGE_NAME = "ru.hennery.elements.alerts.drivers";
	
	public static List<DriverDesc> getDrivers() throws Exception {
		return DriverFactoryBase.getDrivers(SENDER_PACKAGE_NAME);
	}
	
	public static Driver getDriver(String driverClassName) throws Exception {
		return (Driver) DriverFactoryBase.getDriver(driverClassName, SENDER_PACKAGE_NAME);
	}
	
}

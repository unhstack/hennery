package ru.hennery.services.results;

import ru.hennery.elements.scripts.Script;

public class JsonScript extends JsonResult  {
	public Script script;
	public String cron;
	
	public static JsonScript OK(Script script, String cron) {
		JsonScript js = (JsonScript) OK(JsonScript.class);
		js.script = script;
		js.cron = cron;
		
		return js;
	}
	public static JsonScript ERROR(String msg, Object ... args) {
		return (JsonScript) ERROR(msg, JsonScript.class, args);
	}
}

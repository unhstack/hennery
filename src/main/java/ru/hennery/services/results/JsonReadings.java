package ru.hennery.services.results;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlList;

import ru.hennery.elements.sensors.Reading;

public class JsonReadings extends JsonResult {
	
	public List<Reading> result;
	
	
	public BigDecimal[][] d;
	
	public JsonReadings() {
		
	}
	
	public static JsonReadings OK(List<Reading> readings) {
		JsonReadings r = (JsonReadings) JsonResult.OK(JsonReadings.class);
		r.result = readings;
		return r;
	}
	
	public static JsonReadings OK(BigDecimal[][] readings) {
		JsonReadings r = (JsonReadings) JsonResult.OK(JsonReadings.class);
		r.d = readings;
		return r;
	}
	
	public static JsonReadings ERROR(String msg, Object ... args) {
		return (JsonReadings) JsonResult.ERROR(msg, JsonReadings.class, args);
	}

}

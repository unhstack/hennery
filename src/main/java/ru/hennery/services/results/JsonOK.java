package ru.hennery.services.results;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonOK {
    private String result;

    public JsonOK() {

    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}

package ru.hennery.services.results;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import ru.hennery.elements.alerts.Alert;

public class JsonAlerts extends JsonResult {
	@XmlElement
	public List<Alert> result;
	public List<JsonLabelValue> labelValue;
	
	public JsonAlerts() {
		
	}
	
	public static JsonAlerts OK(List<Alert> alerts) {
		JsonAlerts js = (JsonAlerts) JsonResult.OK(JsonAlerts.class);
		js.result = alerts;
		return js;
	}

	public static JsonAlerts OKLabelValue(List<JsonLabelValue> switches) {
		JsonAlerts js = (JsonAlerts) JsonResult.OK(JsonAlerts.class);
		js.labelValue = switches;
		return js;
	}
}

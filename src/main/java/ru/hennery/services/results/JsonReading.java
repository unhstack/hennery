package ru.hennery.services.results;

import ru.hennery.elements.sensors.Reading;

public class JsonReading extends JsonResult {
	public Reading result;
	
	public JsonReading() {
		
	}
	public static JsonReading OK(Reading reading) {
		JsonReading r = (JsonReading) JsonResult.OK(JsonReading.class);
		r.result = reading;
		return r;		
	}
	
	public static JsonReading ERROR(String msg, Object ... args) {
		return (JsonReading) JsonResult.ERROR(msg, JsonReading.class, args);
	}
}

package ru.hennery.services.results;

import ru.hennery.elements.switches.Switch;

public class JsonSwitch extends JsonResult {
	public Switch sw;
	public String cronOn;
	public String cronOff;
	
	public static JsonSwitch OK(Switch sw, String cronOn, String cronOff) {
		JsonSwitch js = (JsonSwitch) OK(JsonSwitch.class);
		js.sw = sw;
		js.cronOn = cronOn;
		js.cronOff = cronOff;
		return js;
	}
	public static JsonSwitch ERROR(String msg, Object ... args) {
		JsonSwitch js = (JsonSwitch) ERROR(msg, JsonSwitch.class, args);		
		return js;
	}
}

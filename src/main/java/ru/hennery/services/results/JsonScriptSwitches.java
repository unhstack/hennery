package ru.hennery.services.results;

import java.util.List;

public class JsonScriptSwitches extends JsonResult {
	public List<JsonScriptSwitch> result;
	
	public static class JsonScriptSwitch {
		public Long idSwitch;
		public String name;
		public String scriptName;
		
		public JsonScriptSwitch() {}
		
		public JsonScriptSwitch(Long idSwitch, String name, String scriptName) {
			this.idSwitch = idSwitch;
			this.name = name;
			this.scriptName = scriptName;
		}

	}
	
	public static JsonScriptSwitches OK(List<JsonScriptSwitch> sensors) {
		JsonScriptSwitches  js = (JsonScriptSwitches) JsonResult.OK(JsonScriptSwitches.class);
		js.result = sensors;
		return js;
	}
	
	public static JsonScriptSwitches ERROR(String msg, Object ... args) {
		JsonScriptSwitches js = (JsonScriptSwitches) ERROR(msg, JsonScriptSwitches.class, args);		
		return js;
	}
	
}

package ru.hennery.services.results;

import java.util.List;

import ru.hennery.elements.scripts.Script;
public class JsonScripts extends JsonResult {
	
	public List<Script> result;
	
	public JsonScripts() {
		
	}
	
	public static JsonScripts OK(List<Script> script) {
		JsonScripts js = (JsonScripts) JsonResult.OK(JsonScripts.class);
		js.result = script;
		return js;
	}

}

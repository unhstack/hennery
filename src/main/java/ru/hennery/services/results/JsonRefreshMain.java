package ru.hennery.services.results;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import ru.hennery.elements.scripts.Script;
import ru.hennery.elements.sensors.Sensor;
import ru.hennery.elements.switches.Switch;

public class JsonRefreshMain extends JsonResult {
	public static class Result {
		public LocalDateTime timer;
		public List<Sensor> sensors;
		public List<Switch> switches;
		public List<Script> scripts;		
	}
	
	public Result result;
	
	public static JsonRefreshMain OK (Result result) {
		JsonRefreshMain jrm = (JsonRefreshMain) OK(JsonRefreshMain.class);
		jrm.result = result;
		return jrm;
	}
	
	public static JsonRefreshMain ERROR(String msg, Object ... args) {
		JsonRefreshMain jrm = (JsonRefreshMain) ERROR(msg, JsonRefreshMain.class, args);
		return jrm;
	}
}

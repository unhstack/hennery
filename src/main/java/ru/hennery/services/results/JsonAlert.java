package ru.hennery.services.results;

import ru.hennery.elements.alerts.Alert;

public class JsonAlert extends JsonResult {
	public Alert alert;
	public String cron;
	
	public static JsonAlert OK(Alert alert, String cron) {
		JsonAlert ja = (JsonAlert) OK(JsonAlert.class);
		ja.alert = alert;
		ja.cron = cron;
		return ja;		
	}
	public static JsonAlert ERROR(String msg, Object ... args) {
		JsonAlert ja = (JsonAlert) ERROR(msg, JsonAlert.class, args);		
		return ja;
	}
}

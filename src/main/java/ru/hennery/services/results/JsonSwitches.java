package ru.hennery.services.results;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import ru.hennery.elements.switches.Switch;

public class JsonSwitches extends JsonResult{
	@XmlElement
	public List<Switch> result;
	public List<JsonLabelValue> labelValue;
	
	public JsonSwitches() {
		
	}
	
	public static JsonSwitches OK(List<Switch> switches) {
		JsonSwitches js = (JsonSwitches) JsonResult.OK(JsonSwitches.class);
		js.result = switches;
		return js;
	}

	public static JsonSwitches OKLabelValue(List<JsonLabelValue> switches) {
		JsonSwitches js = (JsonSwitches) JsonResult.OK(JsonSwitches.class);
		js.labelValue = switches;
		return js;
	}
}

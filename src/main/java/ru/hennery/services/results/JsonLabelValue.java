package ru.hennery.services.results;

public class JsonLabelValue {
	public String label;
	
	public String value;
	
	public JsonLabelValue() {
		
	}
	public JsonLabelValue(String label, String value) {
		this.label = label;
		this.value = value;
	}
}

package ru.hennery.services.results;

import java.math.BigDecimal;
import java.util.List;

import ru.hennery.elements.sensors.Reading;

public class JsonFlotData extends JsonResult {
	public String label;
	public BigDecimal[][] data;
	
	public JsonFlotData(){
		
	}
	
	public static JsonFlotData OK(List<Reading> readings) {
		JsonFlotData fd = (JsonFlotData) OK(JsonFlotData.class);
		fd.data = new BigDecimal[readings.size()][2];
		int i = 0;
		for (Reading rd : readings) {
			fd.data[i][0] = rd.getValue();
			fd.data[i][0] = rd.getValue();
			i++;
		}
		return fd;
	}
}

package ru.hennery.services.results;

import ru.hennery.elements.sensors.Sensor;

public class JsonSensor extends JsonResult {
	public Sensor sensor;
	public String cron;
	
	public static JsonSensor OK(Sensor sensor, String cron) {
		JsonSensor js = (JsonSensor) OK(JsonSensor.class);
		js.sensor = sensor;
		js.cron = cron;
		return js;		
	}
	public static JsonSensor ERROR(String msg, Object ... args) {
		JsonSensor js = (JsonSensor) ERROR(msg, JsonSensor.class, args);		
		return js;
	}
}

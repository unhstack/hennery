package ru.hennery.services.results;

import java.util.List;

public class JsonScriptSensors extends JsonResult {
	public List<JsonScriptSensor> result;
	
	public static class JsonScriptSensor {
		public Long idSensor;
		public String name;
		public String scriptName;
		
		public JsonScriptSensor() {}
		
		public JsonScriptSensor(Long idSensor, String name, String scriptName) {
			this.idSensor = idSensor;
			this.name = name;
			this.scriptName = scriptName;
		}

	}
	
	public static JsonScriptSensors OK(List<JsonScriptSensor> sensors) {
		JsonScriptSensors  ms = (JsonScriptSensors) JsonResult.OK(JsonScriptSensors.class);
		ms.result = sensors;
		return ms;
	}
	
	public static JsonScriptSensors ERROR(String msg, Object ... args) {
		JsonScriptSensors ms = (JsonScriptSensors) ERROR(msg, JsonScriptSensors.class, args);		
		return ms;
	}
	
}

package ru.hennery.services.results;



import ru.hennery.elements.alerts.Alert;
import ru.hennery.elements.scripts.Script;
import ru.hennery.elements.sensors.Sensor;
import ru.hennery.elements.switches.Switch;

public class JsonResult {
	
	public enum Stat {OK, ERROR};
	
	public Stat stat = null;
	public String msg = null;
	
	public Sensor sensor = null;
	public Switch sw = null;
	public Script script = null;
	public Alert alert = null;
	
	
	public JsonResult() {
		
	}
	protected static JsonResult OK(Class<? extends JsonResult> retClass) {
		JsonResult jr = null;
		try {
			jr = retClass.getConstructor().newInstance();
		} catch (Exception e) {
			
		}
		jr.stat = Stat.OK;
		return jr;
	}
	
	protected static JsonResult OK(String msg, Class<? extends JsonResult> retClass, Object ... args) {
		JsonResult jr = OK(retClass);		
		jr.msg = msg == null ? null : String.format(msg,  args);
		return jr;
	}
	
	public static JsonResult OK(String msg, Object ... args) {
		return OK(msg, JsonResult.class, args);
	}
	
	public static JsonResult OK(Sensor result) {	
		JsonResult jr = OK(JsonResult.class);
		jr.sensor = result;
		return jr;
	}
	
	public static JsonResult OK(Switch result) {	
		JsonResult jr = OK(JsonResult.class);
		jr.sw = result;
		return jr;
	}	
	
	public static JsonResult OK(Script result) {	
		JsonResult jr = OK(JsonResult.class);
		jr.script = result;
		return jr;
	}	
	
	public static JsonResult OK(Alert result) {	
		JsonResult jr = OK(JsonResult.class);
		jr.alert = result;
		return jr;
	}
		

	protected static JsonResult ERROR(String msg, Class<? extends JsonResult> retClass, Object ... args) {
		JsonResult jr = null;
		try {
			jr = retClass.getConstructor().newInstance();
		} catch (Exception e) {
			
		}
		jr.stat = Stat.ERROR;
		jr.msg = String.format(msg, args);
		return jr;
	}
	
	public static JsonResult ERROR(String msg, Object ... args) {
		return ERROR(msg, JsonResult.class, args);	
	}
}

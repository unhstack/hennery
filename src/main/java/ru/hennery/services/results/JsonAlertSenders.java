package ru.hennery.services.results;

import java.util.List;
import java.util.Map;

import ru.hennery.elements.alerts.Driver;

public class JsonAlertSenders extends JsonResult {
	
	public Map<String, Driver> result;
	
	public JsonAlertSenders() {
		
	}
	
	public static JsonAlertSenders OK(Map<String, Driver> senders) {
		JsonAlertSenders jas = (JsonAlertSenders) JsonResult.OK(JsonAlertSenders.class);
		jas.result = senders;
		return jas;
	}

	public static JsonAlertSenders ERROR(String msg, Object ... args) {
		JsonAlertSenders jas = (JsonAlertSenders) ERROR(msg, JsonAlertSenders.class, args);		
		return jas;
	}
	
}

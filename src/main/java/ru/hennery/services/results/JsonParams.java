package ru.hennery.services.results;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

public class JsonParams extends JsonResult {
	@XmlElement
	//public List<JsonLabelValue> result;
	public Map<String, String> result;
	
	public static JsonParams OK(Map<String, String> params) {	
		JsonParams sr = (JsonParams) OK(JsonParams.class);
		sr.result = params;
		return sr;
	}
	
	public static JsonParams ERROR(String msg, Object ... args) {
		return (JsonParams) JsonResult.ERROR(msg, JsonParams.class, args);
	}
	
}

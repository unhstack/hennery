package ru.hennery.services.results;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.hennery.elements.sensors.Sensor;

@XmlRootElement
public class JsonSensors extends JsonResult {
	@XmlElement
	public List<Sensor> result;
	public List<JsonLabelValue> labelValue;
	
	public JsonSensors() {
		
	}
	
	public static JsonSensors OK(List<Sensor> sensors) {
		JsonSensors js = (JsonSensors) OK(JsonSensors.class);
		js.result = sensors;
		return js;
	}
	public static JsonSensors OKLabelValue(List<JsonLabelValue> sensors) {
		JsonSensors js = (JsonSensors) OK(JsonSensors.class);
		js.labelValue = sensors;
		return js;
	}
}

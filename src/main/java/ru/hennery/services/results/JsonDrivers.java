package ru.hennery.services.results;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import ru.hennery.elements.DriverFactoryBase;

@XmlRootElement
public class JsonDrivers extends JsonResult {
	
	public List<DriverFactoryBase.DriverDesc> result;
	public List<JsonLabelValue> labelValue;	
	
	public JsonDrivers() {
		
	}
	
	public static JsonDrivers OK(List<DriverFactoryBase.DriverDesc> drivers) {
		JsonDrivers jd = (JsonDrivers) JsonResult.OK(JsonDrivers.class);
		jd.result = drivers;
		return jd;
	}
	
	public static JsonDrivers OKLabelValue(List<JsonLabelValue> drivers) {
		JsonDrivers jd = (JsonDrivers) JsonResult.OK(JsonDrivers.class);
		jd.labelValue = drivers;
		return jd;
	}
	
		
}

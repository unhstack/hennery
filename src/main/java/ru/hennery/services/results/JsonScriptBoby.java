package ru.hennery.services.results;

public class JsonScriptBoby extends JsonResult {	
	public String result;
	
	public JsonScriptBoby(String script) {
		this.result = script;		
	}
	
	public JsonScriptBoby() {
		
	}
	
	public static JsonScriptBoby OK(String script) {
		JsonScriptBoby js = (JsonScriptBoby) JsonResult.OK(JsonResult.class);
		js.result = script;
		return js;
	}
	
	public static JsonScriptBoby ERROR(String msg, Object ... args) {
		JsonScriptBoby js = (JsonScriptBoby) JsonResult.ERROR(msg, JsonResult.class, args);		
		return js;
	}
}

package ru.hennery.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ru.hennery.App;
import ru.hennery.elements.alerts.Alert;
import ru.hennery.elements.alerts.DBAlert;
import ru.hennery.elements.alerts.Driver;
import ru.hennery.elements.alerts.JobAlert;
import ru.hennery.elements.alerts.DriverFactory;
import ru.hennery.elements.switches.Switch;
import ru.hennery.services.results.JsonAlert;
import ru.hennery.services.results.JsonAlertSenders;
import ru.hennery.services.results.JsonAlerts;
import ru.hennery.services.results.JsonDrivers;
import ru.hennery.services.results.JsonLabelValue;
import ru.hennery.services.results.JsonParams;
import ru.hennery.services.results.JsonResult;
import ru.hennery.services.utils.ServIt;

@Path("/alerts")
public class ServAlert extends ServIt {
	static final String JOB_GROUP = "Alerts";
	static final String JOB_NAME  = "alert";
	
	private static final String MSG_ALERT_NOT_FOUND = "Сигнал id %d не найден!";
	private static final String MSG_PARAM_MUST_BE = "Параметр %s обязателен!";
	private static final String MSG_ALERT_ALREADY_ACTIVATED =  "Сигнал: %s уже активен!";
	private static final String MSG_ALERT_ALREADY_DEACTIVATED =  "Сигнал: %s уже не активен!";
	private static final String MSG_ALERT_ACTIVATED =  "Сигнал: %s активирован!";
	private static final String MSG_ALERT_DEACTIVATED =  "Сигнал: %s деактивирован!";
	private static final String MSG_ALERT_DELETED =  "Сигнал: %s удален!";
	private static final String MSG_ALERT_DRIVER_NOT_FOUND = "Драйвер: %s не найден!";
	private static final String MSG_ALERT_DRIVER_ADDED = "Драйвер: %s добавлен.";
	private static final String MSG_ALERT_DRIVER_REMOVED = "Драйвер: %s удален.";
	private static final String MSG_ALERT_PARAM_ADDED = "Параметр добавлен!";
	private static final String MSG_ALERT_NO_SENDERS = "У сигнала нет отправителей!";
	
	
	@Path("add")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult add(@QueryParam("name") String name,
			@QueryParam("msg") String msg,
			@QueryParam("cron") String cron
			) throws Exception {
		
		if (name == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "name");
		if (msg == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "msg");
		
		Alert a = new DBAlert();
		Long id = a.create(name, msg);
		App.getAlerts().put(a.getId(),a);
		
		if (cron != null) {
			setCron(cron, JOB_NAME, id, JOB_GROUP, JobAlert.class);
		}
		
		return JsonResult.OK(a);
	}
	

	@Path("set")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult set(@QueryParam("id") Long id,
			@QueryParam("name") String name,
			@QueryParam("msg") String msg,
			@QueryParam("cron") String cron
			) throws Exception {
		
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		
		Alert a = App.getAlerts().get(id);
		if (a == null) return JsonResult.ERROR(MSG_ALERT_NOT_FOUND, id);
		
		if (name != null) a.setName(name);		
		if (msg != null) a.setMsg(msg);
		
		if (cron != null) { 
			setCron(cron, JOB_NAME, id, JOB_GROUP, JobAlert.class);
		}
		
		return JsonResult.OK(a) ;
	}
	
	@Path("list_senders")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonAlertSenders listSenders(
			@QueryParam("id") Long id
	) throws Exception {		
		if (id == null ) return JsonAlertSenders.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = App.getAlerts().get(id);
		if (a == null) return JsonAlertSenders.ERROR(MSG_ALERT_NOT_FOUND, id);
		
		return JsonAlertSenders.OK(a.listSenders());
		
	}
	
	@Path("add_sender")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult addSender(
			@QueryParam("id") Long id,
			@QueryParam("driver_name") String driverName) throws Exception {
		
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		if (driverName == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "driver_name");
		
		Alert a = App.getAlerts().get(id);
		if (a == null) return JsonResult.ERROR(MSG_ALERT_NOT_FOUND, id);
		Driver driver = null;
		try {
			driver = DriverFactory.getDriver(driverName);
		} catch (ClassNotFoundException e) {
			return JsonResult.ERROR(MSG_ALERT_DRIVER_NOT_FOUND, driverName);
		}
		a.addSender(driver);
		
		return JsonResult.OK(MSG_ALERT_DRIVER_ADDED, driverName);
	}
	
	@Path("del_sender")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult delSender(
			@QueryParam("id") Long id,
			@QueryParam("driver_name") String driverName) throws Exception {
		
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = App.getAlerts().get(id);
		if (a == null) return JsonResult.ERROR(MSG_ALERT_NOT_FOUND, id);
		Driver driver = null;
		try {
			driver = DriverFactory.getDriver(driverName);
		} catch (ClassNotFoundException e) {
			return JsonResult.ERROR(MSG_ALERT_DRIVER_NOT_FOUND, driverName);
		}
		a.removeSender(driver);
		
		return JsonResult.OK(MSG_ALERT_DRIVER_REMOVED, driverName);
	}
	
	@Path("get")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonAlert get(@QueryParam("id") Long id) throws Exception {
	
		if (id == null) return JsonAlert.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = (Alert) App.getAlerts().get(id);
		if (a == null)  return JsonAlert.ERROR(MSG_ALERT_NOT_FOUND, id);
		
		return JsonAlert.OK(a, getCron(JOB_NAME, id, JOB_GROUP)); 
	}
	
	private JsonResult setActivate(Long id, boolean activate) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = App.getAlerts().get(id);
		String name = a.getName();
		String switchIdent = buildJobIdent(name, id);
		boolean active = a.getActive();
		if (active && activate) 
			return JsonResult.ERROR(MSG_ALERT_ALREADY_ACTIVATED, switchIdent);
		if (!active && !activate) 
			return JsonResult.ERROR(MSG_ALERT_ALREADY_DEACTIVATED, switchIdent);
		
		a.setActive(activate);			
		
		return activate ? JsonResult.OK(MSG_ALERT_ACTIVATED, switchIdent) :
			JsonResult.OK(MSG_ALERT_DEACTIVATED, switchIdent)
			;		

	}
	
	@Path("activate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult activate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, true);
	}
	
	@Path("deactivate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult deactivate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, false);
	}
	
	@Path("alert")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult alert(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = App.getAlerts().get(id);
		if (a.listSenders().size() == 0) return JsonResult.OK(MSG_ALERT_NO_SENDERS);
		a.alert();
		return JsonResult.OK(a);
	}	
	
	@Path("del")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult del(@QueryParam("id") Long id) throws Exception {
		Alert a = App.getAlerts().get(id);
		if (a == null) {
			return JsonResult.ERROR(MSG_ALERT_NOT_FOUND, id);
		}
		String name = a.getName();
		removeJob(a.getName(), id, Switch.JOB_GROUP);
		a.destroy();
		App.getAlerts().remove(id);
		return JsonResult.OK(MSG_ALERT_DELETED, buildJobIdent(name, id));
		
	}
	
	@Path("params")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonParams getParams(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonParams.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = App.getAlerts().get(id);
		if (a == null ) return JsonParams.ERROR(MSG_ALERT_NOT_FOUND, "id");
		
		return JsonParams.OK(a.getParams());		
	}
	
	@Path("get_param")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult getParam(@QueryParam("id") Long id, @QueryParam("name") String name) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = App.getAlerts().get(id);
		if (a == null ) return JsonResult.ERROR(MSG_ALERT_NOT_FOUND, "id");
		
		return JsonResult.OK(a.getParam(name));
	}
	
	@Path("set_param")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult setParam(@QueryParam("id") Long id, @QueryParam("name") String name, @QueryParam("value") String value) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Alert a = App.getAlerts().get(id);
		if (a == null ) return JsonResult.ERROR(MSG_ALERT_NOT_FOUND, "id");
		a.setParam(name, value);
		return JsonResult.OK(MSG_ALERT_PARAM_ADDED);
	}
	
	
	@Path("drivers")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonDrivers getDrivers(@QueryParam("fmt") String fmt) throws Exception {		
		if ("labelValue".equals(fmt)) {
			return JsonDrivers.OKLabelValue(
				DriverFactory.getDrivers().stream()
					.map(it -> new  JsonLabelValue(it.name, it.className)).collect(Collectors.toList()));	
		}
		return JsonDrivers.OK(DriverFactory.getDrivers());
		
	}
	
	
	@Path("list")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonAlerts getAlerts(@QueryParam("fmt") String fmt) throws Exception {	
		if ("labelValue".equals(fmt)) {
			List<JsonLabelValue> res = new ArrayList<> ();
			for (Alert it :  App.getAlerts().values()) {
				JsonLabelValue lv = new JsonLabelValue(it.getName(), it.getId().toString()); //getName throws Exceptions
				res.add(lv);
			}
			return JsonAlerts.OKLabelValue(res);
		}
		return JsonAlerts.OK(new ArrayList<Alert>(App.getAlerts().values()));
	}
	
}

package ru.hennery.services;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;












import ru.hennery.App;
import ru.hennery.elements.sensors.DBSensor;
import ru.hennery.elements.sensors.DriverFactory;
import ru.hennery.elements.sensors.JobSensor;
import ru.hennery.elements.sensors.Reading;
import ru.hennery.elements.sensors.Sensor;
import ru.hennery.services.results.JsonDrivers;
import ru.hennery.services.results.JsonReading;
import ru.hennery.services.results.JsonReadings;
import ru.hennery.services.results.JsonSensor;
import ru.hennery.services.results.JsonSensors;
import ru.hennery.services.results.JsonResult;
import ru.hennery.services.utils.ServIt;
import ru.hennery.services.results.JsonLabelValue;

@Path("/sensors")
public class ServSensor extends ServIt{
	

	
	private static final String MSG_SENS_NOT_FOUND = "Сенсор id %d не найден!";
	private static final String MSG_PARAM_MUST_BE = "Параметр %s обязателен!";
	private static final String MSG_SENS_ALREADY_ACTIVATED =  "Сенсор: %s уже активен!";
	private static final String MSG_SENS_ALREADY_DEACTIVATED =  "Сенсор: %s уже не активен!";
	private static final String MSG_SENS_ACTIVATED =  "Сенсор: %s активирован!";
	private static final String MSG_SENS_DEACTIVATED =  "Сенсор: %s деактивирован!";
	private static final String MSG_SENS_DELETED =  "Сенсор: %s удален!";

	
	@Path("add")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult add(@QueryParam("name") String name,
			@QueryParam("driver_name") String driverName,
			@QueryParam("cron") String cron
			) throws Exception {
		
		if (name == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "name");
		if (driverName == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "driver_name");
		
		Sensor s = new DBSensor();
		Long id = s.create(name, driverName);
		App.getSensors().put(s.getId(),s);
		
		if (cron != null) {
			setCron(cron, Sensor.JOB_NAME, id, Sensor.JOB_GROUP, JobSensor.class);
		}
		
		return JsonResult.OK(s);
	}
	
	@Path("set")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult set(@QueryParam("id") Long id,
			@QueryParam("name") String name,
			@QueryParam("cron") String cron,
			@QueryParam("driver_name") String driverName
			) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		
		Sensor s = App.getSensors().get(id);
		if (s == null) return JsonResult.ERROR(MSG_SENS_NOT_FOUND, id);
		
		if (cron != null) { 
			setCron(cron, Sensor.JOB_NAME, id, Sensor.JOB_GROUP, JobSensor.class);
		}
		if (name != null) {
			s.setName(name);
		}
		if (driverName != null) {
			s.setDriverName(driverName);
		}
		return JsonResult.OK(s) ;
	}
	
	@Path("get")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonSensor get(@QueryParam("id") Long id) throws Exception {
	
		if (id == null) return JsonSensor.ERROR(MSG_PARAM_MUST_BE, "id");
		Sensor s = App.getSensors().get(id);
		if (s == null)  return JsonSensor.ERROR(MSG_SENS_NOT_FOUND, id);
		
		return JsonSensor.OK(s, getCron(Sensor.JOB_NAME, id, Sensor.JOB_GROUP)); 
	}
	
	private JsonResult setActivate(Long id, boolean activate) throws Exception {
		if (id == null) return  JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Sensor s = App.getSensors().get(id);
		if (s == null) return JsonResult.ERROR(MSG_SENS_NOT_FOUND, id);
		String name = s.getName();
		String sensorIdent = buildJobIdent(name, id);
		boolean active = s.getActive();
		if (active && activate) 
			return JsonResult.ERROR(MSG_SENS_ALREADY_ACTIVATED, sensorIdent);
		if (!active && !activate) 
			return JsonResult.ERROR(MSG_SENS_ALREADY_DEACTIVATED, sensorIdent);
		
		s.setActive(activate);			
		
		return activate ? JsonResult.OK(MSG_SENS_ACTIVATED, sensorIdent) :
			JsonResult.OK(MSG_SENS_DEACTIVATED, sensorIdent)
			;		
	}
	
	@Path("activate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult activate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, true);
	}
	
	@Path("deactivate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult deactivate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, false);
	}
	
	@Path("del")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult del(@QueryParam("id") Long id) throws Exception {
		if (id == null) return  JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Sensor s = App.getSensors().get(id);
		if (s == null) return JsonResult.ERROR(MSG_SENS_NOT_FOUND, id);
		
		String name = s.getName();
		removeJob(s.getName(), id, Sensor.JOB_GROUP);
		s.destroy();
		App.getSensors().remove(id);
		return JsonResult.OK(MSG_SENS_DELETED, buildJobIdent(name, id));
		
	}
	
	@Path("drivers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JsonDrivers getDrivers(@QueryParam("fmt") String fmt) throws Exception {
		if ("labelValue".equals(fmt)) {			
			return JsonDrivers.OKLabelValue(
				DriverFactory.getDrivers().stream()
					.map(it -> new  JsonLabelValue(it.name, it.className)).collect(Collectors.toList()));		
		}
		return JsonDrivers.OK(DriverFactory.getDrivers());
	}
	
	@Path("list")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonSensors getSensors(@QueryParam("fmt") String fmt) throws Exception {		
		if ("labelValue".equals(fmt)) {
			List<JsonLabelValue> res = new ArrayList<> ();
			for (Sensor it :  App.getSensors().values()) {
				JsonLabelValue lv = new JsonLabelValue(it.getName(), it.getId().toString()); //getName throws Exceptions
				res.add(lv);
			}
			return JsonSensors.OKLabelValue(res);
		}
		return JsonSensors.OK(new ArrayList<Sensor>(App.getSensors().values()));
	}
	
	@Path("readings")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonReadings getReadings(@QueryParam("id") Long id, @QueryParam("fmt") String fmt) throws Exception {
		if (id == null) return 
				JsonReadings.ERROR(MSG_PARAM_MUST_BE, "id");
		
		Sensor s = App.getSensors().get(id);
		if (s == null) return JsonReadings.ERROR(MSG_SENS_NOT_FOUND, id);
		
		List<Reading> readings = s.getReadings();
		if ("flot".equals(fmt)) {
			
			BigDecimal[][] d = 
					readings.stream()
				.map(it -> new BigDecimal[] { new BigDecimal(it.getDt().getTime()), it.getValue() })
				.toArray(BigDecimal[][]::new);
						
			return JsonReadings.OK(d);
		}
		return JsonReadings.OK(readings);
	}
	
	@Path("read")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonReading read(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonReading.ERROR(MSG_PARAM_MUST_BE, "id");
		Sensor s = App.getSensors().get(id);
		if (s == null) return JsonReading.ERROR(MSG_SENS_NOT_FOUND, id);
		
		return JsonReading.OK(s.read());
	}
	
}

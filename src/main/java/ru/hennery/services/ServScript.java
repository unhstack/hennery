package ru.hennery.services;

import java.util.ArrayList;








import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;





















import ru.hennery.App;
import ru.hennery.db.DS;
import ru.hennery.elements.scripts.DBScript;
import ru.hennery.elements.scripts.JobScript;
import ru.hennery.elements.scripts.Script;
import ru.hennery.elements.sensors.JobSensor;
import ru.hennery.elements.sensors.Sensor;
import ru.hennery.elements.switches.Switch;
import ru.hennery.services.results.JsonScript;
import ru.hennery.services.results.JsonScriptSensors;
import ru.hennery.services.results.JsonScriptSwitches;
import ru.hennery.services.results.JsonScripts;
import ru.hennery.services.results.JsonScriptBoby;
import ru.hennery.services.results.JsonResult;
import ru.hennery.services.utils.ServIt;


@Path("/scripts")
public class ServScript  extends ServIt {
	private static final String JOB_GROUP = "Scripts";
	private static final String JOB_NAME  = "script";
	
	private static final String MSG_SCRIPT_NOT_FOUND =  "Скрипт id %d не найден!";
	private static final String MSG_SWITCH_NOT_FOUND =  "Переключатель id %d не найден!";
	private static final String MSG_SENS_NOT_FOUND = "Сенсор id %d не найден!";
	private static final String MSG_PARAM_MUST_BE =  "Параметр %s обязателен!";
	private static final String MSG_SCRIPT_ALREADY_ACTIVATED =  "Скрипт: %s уже активен!";
	private static final String MSG_SCRIPT_ALREADY_DEACTIVATED =  "Скрипт: %s уже не активен!";
	private static final String MSG_SCRIPT_ACTIVATED =  "Скрипт: %s активирован!";
	private static final String MSG_SCRIPT_DEACTIVATED =  "Скрипт: %s деактивирован!";
	private static final String MSG_SCRIPT_DELETED =  "Скрипт: %s удален!а";
	
	private static final String MSG_SENS_ADD_OK = "Сенсор id: %s добавлен.";
	private static final String MSG_SENS_REMOVE_OK = "Сенсор id: %s удален.";
	private static final String MSG_SWITCH_ADD_OK = "Переключатель id: %s добавлен.";
	private static final String MSG_SWITCH_REMOVE_OK = "Переключатель id: %s удален.";
	private static final String MSG_SCRIPT_BODY_ADDED = "Тело скрипта добавлено.";

	@Path("add")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult add(@QueryParam("name") String name,
			@QueryParam("cron") String cron
			) throws Exception {
		if (name == null )	return JsonResult.ERROR(MSG_PARAM_MUST_BE, "name");
		
		Script s = new DBScript();
		Long id = s.create(name);
		App.getScripts().put(s.getId(), s);
		
		if (cron != null) { 
			setCron(cron, JOB_NAME, id, JOB_GROUP, JobScript.class);
		}
		return JsonResult.OK(s) ;
	}

	@Path("set")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult set(@QueryParam("id") Long id,
			@QueryParam("name") String name,
			@QueryParam("cron") String cron,
			@QueryParam("scriptBody") String script
			) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		
		Script s = App.getScripts().get(id);
		if (s == null) return JsonResult.ERROR(MSG_SENS_NOT_FOUND, id);
		
		if (cron != null) { 
			setCron(cron, JOB_NAME, id, JOB_GROUP, JobScript.class);
		}
		if (name != null) {
			s.setName(name);
		}
		if (script != null) {
			s.setScriptBody(script);
		}
		return JsonResult.OK(s) ;
	}
	
	
	@Path("add_sensor")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult addSensor(@QueryParam("id") Long id, 
			@QueryParam("id_sensor") Long idSensor,
			@QueryParam("script_name") String scriptName) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		if (idSensor == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id_sensor");
		if (scriptName == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "script_name");
		
		Script m = App.getScripts().get(id);
		if (m == null) return JsonResult.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		Sensor s = App.getSensors().get(idSensor);
		if (s == null) return JsonResult.ERROR(MSG_SENS_NOT_FOUND, id);
		m.addSensor(s, scriptName);
		return JsonResult.OK(MSG_SENS_ADD_OK, s.getId());
	}
	
	@Path("remove_sensor")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult removeSensor(@QueryParam("id") Long id, @QueryParam("id_sensor") Long idSensor) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		if (idSensor == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id_sensor");
		Script m = App.getScripts().get(id);
		if (m == null) return JsonResult.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		Sensor s = App.getSensors().get(idSensor);
		if (s == null) return JsonResult.ERROR(MSG_SENS_NOT_FOUND, id);
		m.removeSensor(s);
		return JsonResult.OK(MSG_SENS_REMOVE_OK, s.getId());
	}

	@Path("add_switch")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult addSwitch(@QueryParam("id") Long id, 
			@QueryParam("id_switch") Long idSwitch,
			@QueryParam("script_name") String scriptName) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		if (idSwitch == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id_switch");	
		if (scriptName == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "script_name");	
		
		Script m = App.getScripts().get(id);
		if (m == null) return JsonResult.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		Switch sw = App.getSwitches().get(idSwitch);
		if (sw == null) return JsonResult.ERROR(MSG_SWITCH_NOT_FOUND, id);
		m.addSwitch(sw, scriptName);
		return JsonResult.OK(MSG_SWITCH_ADD_OK, sw.getId());
	}

	@Path("remove_switch")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult removeSwitch(@QueryParam("id") Long id, @QueryParam("id_switch") Long idSwitch) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		if (idSwitch == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id_switch");	
		Script m = App.getScripts().get(id);
		if (m == null) return JsonResult.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		Switch sw = App.getSwitches().get(idSwitch);
		if (sw == null) return JsonResult.ERROR(MSG_SWITCH_NOT_FOUND, idSwitch);
		m.removeSwitch(sw);
		return JsonResult.OK(MSG_SWITCH_REMOVE_OK, sw.getId());
	}

	@Path("get_script")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonScriptBoby getScript(@QueryParam("id") Long id) throws Exception {
		if (id == null ) return JsonScriptBoby.ERROR(MSG_PARAM_MUST_BE, "id");
		Script m = App.getScripts().get(id);
		if (m == null) return JsonScriptBoby.ERROR(MSG_SCRIPT_NOT_FOUND, id);		
		return JsonScriptBoby.OK(m.getScriptBody());
	}

	
	@Path("set_script")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult setScript(@QueryParam("id") Long id, @QueryParam("script") String script) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		if (script == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "script");
		Script m = App.getScripts().get(id);
		if (m == null) return JsonResult.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		m.setScriptBody(script);
		return JsonResult.OK(MSG_SCRIPT_BODY_ADDED);
	}
	
	@Path("get_cron")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult getCron(@QueryParam("id") Long id) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Sensor s = App.getSensors().get(id);
		if (s == null) return JsonResult.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		
		return JsonResult.OK(super.getCron(Script.JOB_NAME, id, JOB_GROUP));
	}
	
	@Path("get")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonScript get(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonScript.ERROR(MSG_PARAM_MUST_BE, "id");
		Script m = App.getScripts().get(id);
		if (m == null) return JsonScript.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		return JsonScript.OK(m, getCron(Script.JOB_NAME, id, JOB_GROUP));		
	}
	
	private JsonResult setActivate(Long id, boolean activate) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Script s = App.getScripts().get(id);
		String name = s.getName();
		String switchIdent = buildJobIdent(name, id);
		boolean active = s.getActive();
		if (active && activate) 
			return JsonResult.ERROR(MSG_SCRIPT_ALREADY_ACTIVATED, switchIdent);
		if (!active && !activate) 
			return JsonResult.ERROR(MSG_SCRIPT_ALREADY_DEACTIVATED, switchIdent);
		
		s.setActive(activate);			
		
		return activate ? JsonResult.OK(MSG_SCRIPT_ACTIVATED, switchIdent) :
			JsonResult.OK(MSG_SCRIPT_DEACTIVATED, switchIdent)
			;		
	}
	
	@Path("activate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult activate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, true);
	}
	
	@Path("deactivate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult deactivate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, false);
	}
	
	@Path("run")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult run(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Script ss = App.getScripts().get(id);
		String ssIdent = buildJobIdent(ss.getName(), id);
		if (!ss.getActive()) return JsonResult.ERROR(MSG_SCRIPT_DEACTIVATED, ssIdent);
		ss.run();
		return JsonResult.OK(ss);
	}	
	
	@Path("del")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult del(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Script m = App.getScripts().get(id);
		if (m == null) return JsonResult.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		String name = m.getName();
		removeJob(m.getName(), id, JOB_GROUP);
		m.destroy();
		App.getSensors().remove(id);
		return JsonResult.OK(MSG_SCRIPT_DELETED, buildJobIdent(name, id));
		
	}
	
	@Path("list")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JsonScripts getScripts() {		
		return JsonScripts.OK(new ArrayList<Script>(App.getScripts().values()));
	}

	@Path("list_sensors")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonScriptSensors getSensors(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonScriptSensors.ERROR(MSG_PARAM_MUST_BE, "id");
		Script m = App.getScripts().get(id);
		if (m == null) return JsonScriptSensors.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		
		List<JsonScriptSensors.JsonScriptSensor> res = new ArrayList<>();
		for (Entry<String, Sensor> en : m.getSensors().entrySet()) {
			res.add(new JsonScriptSensors.JsonScriptSensor(en.getValue().getId(), en.getValue().getName(), en.getKey() ) );
		}
		
		return JsonScriptSensors.OK(res);
	}
	@Path("list_switches")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonScriptSwitches getSwitches(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonScriptSwitches.ERROR(MSG_PARAM_MUST_BE, "id");
		Script m = App.getScripts().get(id);
		if (m == null) return JsonScriptSwitches.ERROR(MSG_SCRIPT_NOT_FOUND, id);
		
		List<JsonScriptSwitches.JsonScriptSwitch> res = new ArrayList<>();
		for (Entry<String, Switch> en : m.getSwitches().entrySet()) {
			res.add(new JsonScriptSwitches.JsonScriptSwitch(en.getValue().getId(), en.getValue().getName(), en.getKey() ) );
		}
		return JsonScriptSwitches.OK(res);
	}
}

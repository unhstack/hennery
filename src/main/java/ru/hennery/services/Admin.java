package ru.hennery.services;

import java.io.RandomAccessFile;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;

import ru.hennery.App;
import ru.hennery.db.DS;
import ru.hennery.db.ParseScript;
import ru.hennery.services.results.JsonOK;
import ru.hennery.services.results.JsonResult;
import ru.hennery.services.utils.ServIt;
import ru.hennery.utils.MediaProduces;

/**
 * Админка
 */
@Path("/admin")
public class Admin  extends ServIt{
	private static final Logger logger = LogManager.getLogger(Admin.class.getName());
	private static final String PATH = "admin";
	private static final String INDEX = PATH + "/index.html";
	private static final VelocityContext vcMenu = new VelocityContext();

	/**
	 * Элемент меню для Velocity. Public чтобы шаблонизатор мог его использовать.
	 */
	public static class MnuItem {
		/**
		 * Название пункта меню
		 */
		private String name;
		/**
		 * Ссылка
		 */
		private String link;

		public MnuItem(String name, String link) {
			this.name = name;
			this.link = link;
		}

		public String getName() {
			return name;
		}

		public String getLink() {
			return link;
		}
	}
	@POST
	public Response listPaths(@Context UriInfo uriInfo) {
		return null;
	}
	@GET
	public Response index(@Context UriInfo uriInfo) {
		if (vcMenu.getKeys().length == 0) {

			vcMenu.put("menu",
					new MnuItem[] {
						new MnuItem("DB",   "./admin/db"),
						new MnuItem("Log", "./admin/log"),
						new MnuItem("All jobs", "./admin/jobs"),
						new MnuItem("Switch request log", "./admin/log_requests"),
						new MnuItem("Shutdown", "./admin/shutdown")
					}
			);
		}
		return CLRes.getRes(INDEX, uriInfo, vcMenu);
	}

	@Path("shutdown")
	@GET
	@Produces(MediaProduces.PRODUCES_JSON)
	public JsonResult shutdown() {
		new Thread(() -> {
			try {
				Thread.sleep(10000);
				App.getServer().stop();
			} catch (Exception e) {
				logger.info("Не могу остановить Jetty", e);
			}
		}).start();

		return JsonResult.OK("OK!");
	}

	@Path("ok")
	@GET
	@Produces("application/json")
	public JsonOK ok() {
		JsonOK jsonOK = new JsonOK();
		jsonOK.setResult("OK");
		return jsonOK;
	}

	@Path("db")
	@GET
	public Response db(@QueryParam("sql") String sql, @Context UriInfo uriInfo) throws Exception {
		VelocityContext vc = new VelocityContext();

		if (sql != null) {

			ParseScript ps = new ParseScript(sql);
			int rowAffected = 0;
			StringBuilder sqlResult = new StringBuilder();
			try (DS ds = App.getDS()) {
				String s;
				while ((s = ps.next()) != null) {
					if (s.startsWith("select")) {
						try {
							List<Map<String, Object>> res = ds.sql(s);
							sqlResult.append("<table border='1'>");
							if (res.size() > 0) {
								res.get(0).keySet().forEach(it -> sqlResult.append("<th>").append(it).append("</th>"));
								res.forEach(it -> {
									sqlResult.append("<tr>");
									it.forEach((k, v) -> sqlResult.append("<td>").append(v).append("</td>"));
									sqlResult.append("</tr>");
								});
							}
							sqlResult.append("</table>");
						} catch (SQLException e) {
							sqlResult.append(e.getMessage());
						}

					} else {
						try {
							rowAffected += ds.update(s);
							ds.commit();
						} catch (SQLException e) {
							sqlResult.append(e.getMessage());
						}

					}
				}
			}
			if (rowAffected > 0) sqlResult.append("\nrow affected: ").append(rowAffected);
			return Response.ok(sqlResult.toString()).build();
		}

		return CLRes.getRes("db.html", uriInfo, vc);
	}

	@Path("log")
	@GET
	public Response log(@QueryParam("string_count") Integer stringCount, @Context UriInfo uriInfo) throws Exception {
		VelocityContext vc = new VelocityContext();
		if (stringCount == null) {
			vc.put("stringCount", 300);
		} else {
			vc.put("stringCount", stringCount);
			try(RandomAccessFile raf = new RandomAccessFile("hennery.log", "r")) {

				StringBuffer res = new StringBuffer();

				long pos = raf.length();
				boolean isDone = false;
				while ((pos -= 1)> 0 && stringCount > 0) {
					raf.seek(pos);
					int c = raf.read();
					switch (c) {
					case '\r':
					case '\n':
						if (isDone) break;
						String ln = raf.readLine();
						res.append(ln == null ? "" : ln).append("<br/>");
						isDone = true;
						stringCount --;
						break;
					default:
						isDone = false;
						break;
					}
				}
				res.append(raf.readLine());
				vc.put("text", res.toString());
			}
		}
		return CLRes.getRes("log.html", uriInfo, vc);
	}

	@Path("jobs")
	@GET
	public Response jobs(@Context UriInfo uriInfo) throws Exception {
		VelocityContext vc = new VelocityContext();
		Scheduler scheduler = App.getSched();

		Map<String, Map<String,List<Trigger>>> res = new HashMap<>();

		for (String groupName : scheduler.getJobGroupNames()) {

			Map<String,List<Trigger>> resJob = new HashMap<>();

			for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {

				  String jobName = jobKey.getName();
				  //String jobGroup = jobKey.getGroup();

				  //get job's trigger
				  List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);

				  resJob.put(jobName, triggers);
					  //Date nextFireTime = trigger.getNextFireTime();
				  /*
				  System.out.println("[jobName] : " + jobName + " [groupName] : "
							+ jobGroup + " - " + nextFireTime);
			 */
			}

			res.put(groupName, resJob);
		}
		vc.put("jobs", res);
		return CLRes.getRes("jobs.html", uriInfo, vc);
	}

	@Path("log_requests")
	@GET
	public Response logRequests(@Context UriInfo uriInfo) throws Exception {
		String path = uriInfo.getPath();
		String resp;
		if (path.equalsIgnoreCase("on")) {
			App.getRequestLogHandler().start();
			resp = "Request log on!";
		} else {
			App.getRequestLogHandler().stop();
			resp = "Request log off!";
		}
		return Response.ok(resp).build();
	}
}


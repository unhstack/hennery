package ru.hennery.services.utils;


import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.velocity.VelocityContext;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import ru.hennery.App;
import ru.hennery.services.CLRes;
import ru.hennery.utils.MediaProduces;

public class ServIt {
	protected static final String PRODUCES_JSON = MediaProduces.PRODUCES_JSON;
	private static final String LIST_PATHS_INDEX = "services/index.html";
	private static final String CACHE = "ServsHelp";
	
	public static class PathParam {
		private String name;
		private String type;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
		public PathParam(String name, String type) {
			this.name = name;
			this.type = type;
		}
	}
	
	public static class PathsInfo {
		private String path;
		private List<PathParam> params;

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public List<PathParam> getParams() {
			return params;
		}

		public void setParams(List<PathParam> params) {
			this.params = params;
		}
		
		public PathsInfo(String path, List<PathParam> params) {
			this.path = path;
			this.params = params;
		}

	}
	
	@GET
	public Response listPaths(@Context UriInfo uriInfo) {
		final String cacheKey = "listPathsVC_" + this.getClass().getSimpleName();
		
		Cache cache = CacheManager.getInstance().getCache(CACHE);		
		Element el = null;
		synchronized (this) {
			el = cache.get(cacheKey);
			if (el == null) {
				VelocityContext listPathsVC = new VelocityContext();
				
				List<PathsInfo> result = new ArrayList<>();
				for (Method m : this.getClass().getMethods()) {
					
					Path path = m.getAnnotation(Path.class);
					
					if (path != null) {
						List<PathParam> params = new ArrayList<>();
						int i = 0;
						Annotation[][] aaa = m.getParameterAnnotations();
						Class<?>[] parameterTypes = m.getParameterTypes();
						for (Annotation[] aa : aaa) {
							for (Annotation a : aa) {
								if (a.annotationType().equals(QueryParam.class)) {
									
									params.add(new PathParam(((QueryParam)a).value() ,parameterTypes[i].getSimpleName() ));	
								}
							}
							i++;
						}
						result.add(new PathsInfo(path.value(), params));
					}
				}
				listPathsVC.put("listPaths", result);
				el = new Element(cacheKey, listPathsVC);
				cache.put(el);
			}
		}
		
		return CLRes.getRes(LIST_PATHS_INDEX, uriInfo, (VelocityContext) el.getObjectValue());
	}
	
	public static String buildJobIdent(String name, long id) {		
		return name + " id: " + id;
	}
	
	protected void setCron(String cron, String name, long id, String jobGroup, Class<? extends Job> jobClass) throws SchedulerException {
		JobKey jobKey = new JobKey(buildJobIdent(name, id), jobGroup);
		
		Scheduler scheduler = App.getSched();
		
		CronTrigger trigger = TriggerBuilder.newTrigger()				
				.withSchedule(
						CronScheduleBuilder.cronSchedule(cron).withMisfireHandlingInstructionDoNothing()
				)				
				.build();
		
		if (scheduler.checkExists(jobKey)) {
			Trigger jobTrigger = scheduler.getTriggersOfJob(jobKey).get(0);
			scheduler.rescheduleJob(jobTrigger.getKey(), trigger);
		} else {
			JobDetail jobd = JobBuilder.newJob(jobClass).withIdentity(jobKey)
					.usingJobData("id", id)
					.build();
			App.getSched().scheduleJob(jobd, trigger);
		}
		
	}
	
	protected String getCron(String name, long id, String jobGroup ) throws SchedulerException {
		JobKey jobKey = new JobKey(buildJobIdent(name, id), jobGroup);
		Scheduler scheduler = App.getSched();
		if (!scheduler.checkExists(jobKey)) return null;
		CronTrigger jobTrigger = (CronTrigger) scheduler.getTriggersOfJob(jobKey).get(0);
		return jobTrigger.getCronExpression();
	}
	
	protected void removeJob(String name, Long id, String jobGroup) throws SchedulerException {
		String sensorIdent = buildJobIdent(name, id);
		JobKey jobKey = new JobKey(sensorIdent, jobGroup);
		if (App.getSched().checkExists(jobKey)) {
			App.getSched().deleteJob(jobKey);
		}
	}
}

package ru.hennery.services.utils;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;

//@Provider
public class MOXyJsonContextResolver implements ContextResolver<MoxyJsonConfig> {
	private final MoxyJsonConfig config;
	
	public MOXyJsonContextResolver() {
        config = new MoxyJsonConfig(true);
        config.setIncludeRoot(false);
        config.marshallerProperty(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);
    }
	@Override
	public MoxyJsonConfig getContext(Class<?> type) {
		return config;
	}
	
}


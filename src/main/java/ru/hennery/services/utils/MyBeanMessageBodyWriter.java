package ru.hennery.services.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.eclipse.persistence.jaxb.JAXBContextProperties;

import ru.hennery.services.results.JsonReadings;

//https://bugs.eclipse.org/bugs/show_bug.cgi?id=389815

@Produces(MediaType.APPLICATION_JSON)
//@Provider
public class MyBeanMessageBodyWriter implements MessageBodyWriter<JsonReadings> {

    @Context
    protected Providers providers;
    
	@Override
	public long getSize(JsonReadings arg0, Class<?> arg1, Type arg2,
			Annotation[] arg3, MediaType arg4) {
		// deprecated by JAX-RS 2.0 and ignored by Jersey runtime
		return 0;
	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] arg2,
			MediaType arg3) {
		// TODO Auto-generated method stub
		return type == JsonReadings.class;
	}

	@Override
	public void writeTo(JsonReadings data, Class<?> type, Type genericType,
			Annotation[] annotation, MediaType mediaType,
			MultivaluedMap<String, Object> arg5, OutputStream entityStream)
			throws IOException, WebApplicationException {
		/*
		StringBuilder out = new StringBuilder();
		out.append('[');
		for(BigDecimal[] arr : data.d) {
			out.append('[');
			boolean more = false;
			for (BigDecimal it : arr) {
				if (more) out.append(','); else more = true;
				out.append(it);
			}
			out.append(']');
		}
		out.append(']');
		entityStream.write(out.toString().getBytes());
		*/
		
		 try {
			 	ContextResolver<JAXBContext> resolver 
			 		= providers.getContextResolver(JAXBContext.class, mediaType);
		        JAXBContext jaxbContext;
		        if(null == resolver || null == (jaxbContext = resolver.getContext(type))) {
		            jaxbContext = JAXBContext.newInstance(type);
		        }
		        /*
			    Map<String, Object> properties = new HashMap<String, Object>(2);
		        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
		        //properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, false); 
	            JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] {JsonReadings.class}, properties);
	            */
	            
	            // serialize the entity myBean to the entity output stream
	            Marshaller marshaller = jaxbContext.createMarshaller();
	            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	            marshaller.marshal(data, entityStream);
	       
	        } catch (JAXBException jaxbException) {
	            throw new ProcessingException(
	                "Error serializing a MyBean to the output stream", jaxbException);
	        }
	        
	       
	}

}

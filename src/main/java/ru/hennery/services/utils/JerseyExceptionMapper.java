package ru.hennery.services.utils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class JerseyExceptionMapper implements ExceptionMapper<Exception> {
	private static final Logger logger = LogManager.getLogger(JerseyExceptionMapper.class.getName()); 
	@Override
	public Response toResponse(Exception e) {
		logger.catching(e);
		if (e instanceof WebApplicationException) {
			WebApplicationException we = (WebApplicationException) e;
			return we.getResponse();
		}
		
		return Response.status(500).build();
	}

}

package ru.hennery.services;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.glassfish.jersey.message.internal.MediaTypes;

import ru.hennery.utils.ClassUtils;
import ru.hennery.utils.Options;

@Path("/")
public class CLRes {
	private static final Logger logger = LogManager.getLogger(CLRes.class.getName()); 
	private static final String INDEX = "index.html"; 
	
	@GET
	public Response index(@Context UriInfo uriInfo) throws Exception {		
		return getRes(INDEX, uriInfo, null);		
	}

	@Produces("image/ico")
	@Path("favicon.ico")
	@GET
	public Response favicon(@Context UriInfo uriInfo) throws Exception {		
		return getRes("favicon.ico", uriInfo, null);
	}
	
	@Path("res/{resName: .*}")	
	@GET
	public Response Res(@PathParam("resName") final String resName,
			@Context UriInfo uriInfo) {
		return getRes(resName, uriInfo, null);
	}
	
	private static boolean isVTemplated(String resFile) throws MalformedURLException, URISyntaxException {
		
		/*java.nio.file.Path p = Paths.get(resFile);
		String file = p.getFileName().toString();
		*/
		return resFile.endsWith(".html") || resFile.endsWith(".htm");		
	}
	
	public static Response getRes(final String resName,
			UriInfo uriInfo, VelocityContext vc) {
		
		try {			
				
			final byte[] buf = ClassUtils.getCachedPublicRes(resName);
			if (buf == null) return Response.status(Response.Status.NOT_FOUND).entity("Resource: " + resName + " not found").build();
			
			if (isVTemplated(resName)) {
				if (vc == null) vc = new VelocityContext();
				Options.putPUITheme(vc);
				for(Entry<String, List<String>> entry : uriInfo.getPathParameters(true).entrySet() ){
					String key = entry.getKey();
					String value = entry.getValue().get(0);
					vc.put(key, value);
				}
				StringWriter writer = new StringWriter();
				
				StringReader reader = new StringReader(new String(buf, "UTF-8"));
				
				boolean evaluated = Velocity.evaluate(vc, writer, "log tag", reader);
				logger.debug("Velocity template evaluated {}", evaluated);
				return Response.ok(writer.toString()).build();
				
			}
	
			
			return Response.ok(buf).build();		
			
		} catch (Exception e) {
			logger.catching(e);
		}
		return Response.noContent().build();
	}
}

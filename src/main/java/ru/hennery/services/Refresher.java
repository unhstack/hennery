package ru.hennery.services;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import ru.hennery.App;
import ru.hennery.services.results.JsonRefreshMain;
import ru.hennery.services.utils.ServIt;

@Path("/refresh")
public class Refresher extends ServIt {
	@Path("main")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonRefreshMain refreshMain() {
		JsonRefreshMain.Result res = new JsonRefreshMain.Result();
		res.timer = LocalDateTime.now();
		res.sensors = new ArrayList<>(App.getSensors().values());
		res.switches = new ArrayList<>(App.getSwitches().values());
		res.scripts = new ArrayList<>(App.getScripts().values());
		
		return JsonRefreshMain.OK(res);
	}	
}

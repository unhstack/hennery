package ru.hennery.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ru.hennery.App;
import ru.hennery.elements.switches.DBSwitch;
import ru.hennery.elements.switches.DriverFactory;
import ru.hennery.elements.switches.JobSwitchOff;
import ru.hennery.elements.switches.JobSwitchOn;
import ru.hennery.elements.switches.Switch;
import ru.hennery.services.results.JsonDrivers;
import ru.hennery.services.results.JsonSwitch;
import ru.hennery.services.results.JsonSwitches;
import ru.hennery.services.results.JsonResult;
import ru.hennery.services.utils.ServIt;
import ru.hennery.services.results.JsonLabelValue;

@Path("/switches")
public class ServSwitch extends ServIt{

	private static final String MSG_SWITCH_NOT_FOUND 		 	= "Переключатель id %d не найден!";
	private static final String MSG_PARAM_MUST_BE 			 	= "Параметр %s обязателен!";
	private static final String MSG_SWITCH_ALREADY_ACTIVATED 	= "Переключатель: %s уже активен!";
	private static final String MSG_SWITCH_ALREADY_DEACTIVATED 	= "Переключатель: %s уже не активен!";
	private static final String MSG_SWITCH_ACTIVATED 		 	= "Переключатель: %s активирован!";
	private static final String MSG_SWITCH_DEACTIVATED 		 	= "Переключатель: %s деактивирован!";
	private static final String MSG_SWITCH_DELETED 			 	= "Переключатель: %s удален!";
	
	@Path("add")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult add(@QueryParam("name") String name,
			@QueryParam("driver_name") String driverName,
			@QueryParam("cron_on") String cronOn,
			@QueryParam("cron_off") String cronOff
			) throws Exception {
		if (name == null )		return JsonResult.ERROR(MSG_PARAM_MUST_BE, "name");
		if (driverName == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "driver_name");
		
		Switch s = new DBSwitch();
		Long id = s.create(name, driverName);
		App.getSwitches().put(s.getId(), s);
		
		if (cronOn != null) { 
			setCron(cronOn, Switch.JOB_NAME_ON, id, Switch.JOB_GROUP, JobSwitchOn.class);
		}
		if (cronOff != null) {
			setCron(cronOff, Switch.JOB_NAME_OFF, id, Switch.JOB_GROUP, JobSwitchOff.class);
		}
		return JsonResult.OK(s) ;
	}

	@Path("set")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult set(@QueryParam("id") Long id,
			@QueryParam("name") String name,
			@QueryParam("cron_on") String cronOn,
			@QueryParam("cron_off") String cronOff,
			@QueryParam("driver_name") String driverName
			) throws Exception {
		if (id == null ) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		
		Switch s = App.getSwitches().get(id);
		if (s == null) return JsonResult.ERROR(MSG_SWITCH_NOT_FOUND, id);
		
		if (cronOn != null) { 
			setCron(cronOn, Switch.JOB_NAME_ON, id, Switch.JOB_GROUP, JobSwitchOn.class);
		}
		if (cronOff != null) { 
			setCron(cronOff, Switch.JOB_NAME_OFF, id, Switch.JOB_GROUP, JobSwitchOff.class);
		}
		if (name != null) {
			s.setName(name);
		}
		if (driverName != null) {
			s.setDriverName(driverName);
		}
		return JsonResult.OK(s) ;
	}
	
	@Path("get")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonSwitch get(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonSwitch.ERROR(MSG_PARAM_MUST_BE, "id");
		Switch sw = App.getSwitches().get(id);
		if (sw == null) return JsonSwitch.ERROR(MSG_SWITCH_NOT_FOUND, id);
		return JsonSwitch.OK(sw, getCron(Switch.JOB_NAME_ON, id, Switch.JOB_GROUP), getCron(Switch.JOB_NAME_OFF, id, Switch.JOB_GROUP));		
	}
	
	private JsonResult setActivate(Long id, boolean activate) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Switch s = App.getSwitches().get(id);
		String name = s.getName();
		String switchIdent = buildJobIdent(name, id);
		boolean active = s.getActive();
		if (active && activate) 
			return JsonResult.ERROR(MSG_SWITCH_ALREADY_ACTIVATED, switchIdent);
		if (!active && !activate) 
			return JsonResult.ERROR(MSG_SWITCH_ALREADY_DEACTIVATED, switchIdent);
		
		s.setActive(activate);			
		
		return activate ? JsonResult.OK(MSG_SWITCH_ACTIVATED, switchIdent) :
			JsonResult.OK(MSG_SWITCH_DEACTIVATED, switchIdent)
			;		

	}
	
	@Path("activate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult activate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, true);
	}
	
	@Path("deactivate")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult deactivate(@QueryParam("id") Long id) throws Exception {
		return setActivate(id, false);
	}
	
	@Path("on")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult on(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Switch s = App.getSwitches().get(id);
		String switchIdent = buildJobIdent(s.getName(), id);
		if (!s.getActive()) return JsonResult.ERROR(MSG_SWITCH_DEACTIVATED, switchIdent);
		s.on();
		return JsonResult.OK(s);
	}	

	@Path("off")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult off(@QueryParam("id") Long id) throws Exception {
		if (id == null) return JsonResult.ERROR(MSG_PARAM_MUST_BE, "id");
		Switch s = App.getSwitches().get(id);
		String switchIdent = buildJobIdent(s.getName(), id);
		if (!s.getActive()) return JsonResult.ERROR(MSG_SWITCH_DEACTIVATED, switchIdent);
		s.off();
		return JsonResult.OK(s);
	}	
	
	@Path("del")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonResult del(@QueryParam("id") Long id) throws Exception {
		Switch s = App.getSwitches().get(id);
		if (s == null) {
			return JsonResult.ERROR(MSG_SWITCH_NOT_FOUND, id);
		}
		String name = s.getName();
		removeJob(s.getName(), id, Switch.JOB_GROUP);
		s.destroy();
		App.getSwitches().remove(id);
		return JsonResult.OK(MSG_SWITCH_DELETED, buildJobIdent(name, id));
		
	}
	
	@Path("drivers")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonDrivers getDrivers(@QueryParam("fmt") String fmt) throws Exception {
		if ("labelValue".equals(fmt)) {
			return JsonDrivers.OKLabelValue(
				DriverFactory.getDrivers().stream()
					.map(it -> new  JsonLabelValue(it.name, it.className)).collect(Collectors.toList()));		
		}
		return JsonDrivers.OK(DriverFactory.getDrivers());
	}
	
	@Path("list")
	@GET
	@Produces(PRODUCES_JSON)
	public JsonSwitches getSwitches(@QueryParam("fmt") String fmt) throws Exception {
		if ("labelValue".equals(fmt)) {
			List<JsonLabelValue> res = new ArrayList<> ();
			for (Switch it :  App.getSwitches().values()) {
				JsonLabelValue lv = new JsonLabelValue(it.getName(), it.getId().toString()); //getName throws Exceptions
				res.add(lv);
			}
			return JsonSwitches.OKLabelValue(res);
		}
		return JsonSwitches.OK(new ArrayList<Switch>(App.getSwitches().values()));
	}

}

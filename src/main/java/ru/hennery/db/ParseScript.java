package ru.hennery.db;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Scanner;


public class ParseScript {
	private enum State {SYM, COMM_B, COMM, COMM_E, COMM_LINE_B,COMM_LINE}; 
	
	private Reader r;
	
	private char[] COMM_BEGIN = "/*".toCharArray();
	private char[] COMM_END = "*/".toCharArray();
	private char[] COMM_LINE = "--".toCharArray();
	private char DELIM = ';';
	
	private boolean is_nothing_to_read;
	
	
	public ParseScript(Reader r) {		
		this.r = r;
		is_nothing_to_read = false;
	}

	public ParseScript(String s) {		
		this.r = new StringReader(s);
		is_nothing_to_read = false;
	}
	
	public String next() throws IOException {
		
		if (is_nothing_to_read) return null;
		
		State state = State.SYM;
		StringBuilder sb = new StringBuilder();
		int readed;
		char buf = '\0';	
		char prev_buf;
		while ( (readed =  r.read() ) > 0) {
			buf = (char) readed;
			if (buf == DELIM) break;
			prev_buf = buf;
			switch(state) {
			case SYM:
				if (buf == COMM_BEGIN[0]) {
					state = State.COMM_B;
					break;
				}
			 	if (buf == COMM_LINE[0]) {
			 		state = State.COMM_LINE_B;
					break;
			 	}
			 	sb.append(buf);
				break;			
			case COMM_B:
				if (buf == COMM_BEGIN[1]) { 
					state = State.COMM;
				} else {
					state = State.SYM;
					sb.append(prev_buf);
					sb.append(buf);
				}	
				
				break;
			case COMM:
				if (buf == COMM_END[0]) state = State.COMM_E;
				break;
			case COMM_E:
				if (buf == COMM_END[1]) {
					state = State.COMM_B;
				} else {
					state = State.SYM;
					sb.append(prev_buf);
					sb.append(buf);
				}
				break;
			case COMM_LINE_B:
				if (buf == COMM_LINE[1]) {
					state = State.COMM_LINE;
				} else {
					state = State.SYM;
					sb.append(prev_buf);
					sb.append(buf);
				}
				break;
			case COMM_LINE:
				if (buf == '\n') state = State.SYM;
				if (buf == '\r') state = State.COMM_LINE;
				break;				
			default:
				break;
				
			}
		}
		if (readed < 0 ) is_nothing_to_read = true;
		String s = sb.toString().trim();
		return s.length() > 0 ?  s.replaceAll("\\r|\\n", " ") : null;
	}
	
}

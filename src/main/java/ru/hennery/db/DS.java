package ru.hennery.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;

public interface DS extends AutoCloseable  {
	/**
	 * Инициализация
	 *
	 * @throws Exception в случае ошибки инициализации
	 */
	void init() throws Exception;

	/**
	 * Получение соединения с базой
	 *
	 * @return {@link java.sql.Connection}
	 * @throws SQLException в случае ошибки исполнения
	 */
	Connection getConnection() throws SQLException;

	/**
	 * Выполняет SQL запрос
	 *
	 * @param sql строка запроса
	 * @param arguments аргументы запроса
	 * @return список Map из названий полей и результата
	 * @throws SQLException в случае ошибки исполнения
	 */
	List<Map<String, Object>> sql(String sql, Object...arguments) throws SQLException;

	/**
	 * Выполняет SQL запрос возвращает список бинов
	 *
	 * @param sql строка запроса
	 * @param beanClass класс бина для списка
	 * @param arguments аргументы запроса
	 * @param <T> тип бина
	 * @return список бинов
	 * @throws SQLException в случае ошибки исполнения
	 */
	<T> List<T> sqlBeans(String sql, Class<T> beanClass, Object...arguments) throws SQLException;

	/**
	 * Выполняет SQL запрос возвращает бины собранные itemsCollector
	 *
	 * @param sql строка запроса
	 * @param itemsCollector коллектор для списка бинов
	 * @param beanClass класс бина
	 * @param arguments аргументы запроса
	 * @param <T1> тип списка бинов
	 * @param <T> тип бина
	 * @return список бинов
	 * @throws SQLException в случае ошибки исполнения
	 */
	<T1, T> T1 sqlBeans(String sql, Collector<T, ?, T1> itemsCollector, Class<? extends T> beanClass, Object... arguments) throws SQLException;

	/**
	 * Выполняет DML запрос
	 * @param dml строка запроса
	 * @param arguments аргументы запроса
	 * @return результат выполнения
	 * @throws SQLException в случае ошибки исполнения
	 */
	int update(String dml, Object...arguments) throws SQLException;

	/**
	 * Генерация числового идентификатора записи таблицы
	 *
	 * @param name название таблицы
	 * @return идентификатор
	 * @throws SQLException в случае ошибки исполнения
	 */
	long generateId(String name) throws SQLException;

	/**
	 * Возвращает {@link java.lang.Long } из первого столбца sql запроса
	 *
	 * @param sql строка запроса
	 * @param arguments аргументы запроса
	 * @return {@link java.lang.Long} значение
	 * @throws SQLException в случае ошибки исполнения
	 */
	Long getLong(String sql, Object...arguments)  throws SQLException;

	/**
	 * Возвращает {@link java.lang.String} из первого столбца sql запроса
	 *
	 * @param sql строка запроса
	 * @param arguments аргументы запроса
	 * @return {@link java.lang.String} значение
	 * @throws SQLException в случае ошибки исполнения
	 */
	String getString(String sql, Object...arguments)  throws SQLException;

	/**
	 * Commit транзакции
	 *
	 * @throws SQLException в случае ошибки исполнения
	 */
	void commit() throws SQLException;

	/**
	 * Rollback транзакции
	 *
	 * @throws SQLException в случае ошибки исполнения
	 */
	void rollback() throws Exception;

	/**
	 * Освобождение ресурсов занятых инициализацией
	 *
	 * @throws SQLException в случае ошибки исполнения
	 */
	void free() throws SQLException;
}

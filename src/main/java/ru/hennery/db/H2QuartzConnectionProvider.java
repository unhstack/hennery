package ru.hennery.db;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.utils.ConnectionProvider;

import ru.hennery.App;

public class H2QuartzConnectionProvider implements ConnectionProvider {
	static final Logger logger = LogManager.getLogger(H2QuartzConnectionProvider.class.getName()); 
	

	@Override
	public Connection getConnection() throws SQLException {
		DS ds = null;
		try {
			ds = App.getDS();
		} catch (Exception e) {
			logger.catching(e);
			return null;
		}
		
		return ds.getConnection();
	}

	@Override
	public void initialize() throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void shutdown() throws SQLException {
		// TODO Auto-generated method stub

	}

}

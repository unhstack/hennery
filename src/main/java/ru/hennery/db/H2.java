package ru.hennery.db;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mchange.v2.c3p0.ComboPooledDataSource;

//велосипед...
public class H2 implements DS {
	static final Logger logger = LogManager.getLogger(H2.class.getName()); 
	
	private static ComboPooledDataSource pooledDataSource;
	private Connection c;
		
	private static final String HENNERY_SQL = "hennery.h2.sql";
	private static final String JDBC_URL = "jdbc:h2:./hennery;USER=sa;PASSWORD=123;DB_CLOSE_ON_EXIT=FALSE";

	private static final class BeanCollector<A> implements Collector<Map.Entry<String, Object>,A,A> {

		private Class<? extends A> beanClass;

		public BeanCollector(Class<? extends A> beanClass) {
			this.beanClass = beanClass;
		}

		@Override
		public Supplier<A> supplier() {
			return () -> {
				try {
					return beanClass.getConstructor().newInstance();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			};
		}

		private boolean compareFieldNames(String beanFieldName, String fieldName) {
			if (beanFieldName.equals(fieldName)) {
				return true;
			}
			return beanFieldName.replace("_", "")
					.equalsIgnoreCase(fieldName.replace("_", ""));
		}

		private void fillBeanFields(Class clazz, Object a, Map.Entry<String, Object> t) {
			for (Field field : clazz.getDeclaredFields()) {
				String beanFieldName = field.getName();
				String fieldName = t.getKey();
				Object fieldValue = t.getValue();
				int fieldModifiers = field.getModifiers();
				if (!Modifier.isStatic(fieldModifiers) && compareFieldNames(beanFieldName, fieldName)) {
					boolean fieldAccessible = field.isAccessible();
					try {
						field.setAccessible(true);
						field.set(a, fieldValue);
					} catch (IllegalAccessException e) {
						throw new RuntimeException(e);
					} finally {
						field.setAccessible(fieldAccessible);
					}
				}
			}
			if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class)  ) {
				fillBeanFields(clazz.getSuperclass(), a, t);
			}
		}

		@Override
		public BiConsumer<A, Map.Entry<String, Object>> accumulator() {
			return (a, t) -> fillBeanFields(a.getClass(), a, t);
		}

		@Override
		public BinaryOperator<A> combiner() {
			return (a, a1) -> a;
		}

		@Override
		public Function<A, A> finisher() {
			return (a) -> a;
		}

		@Override
		public Set<Characteristics> characteristics() {
			return null;
		}

	}

	@Override
	public void init() throws Exception {
		if (createTables()) logger.info("DB updated.");	
	}

	@Override
	public void close() throws SQLException {
		if (c != null) c.close();		
	}
	
	private boolean isNewDB(String versionStr) throws SQLException {
		if (!isTableExists("version")) return true; //нет таблицы version - нет базы
		
		List<Map<String, Object>> res = sql("select version from version");
		Map<String, Object> item = res.get(0);
		String currentVersion = (String) item.get("VERSION");
		
		return !versionStr.trim().equals(currentVersion.trim());
	}

	/**
	 * Заполняет параметры запуска PreparedStatement
	 *
	 * @param st заполняемый PreparedStatement
	 * @param arguments параметры
	 * @throws SQLException в случае ошибки заполнения
	 */
	private void fillStatementsParams(PreparedStatement st, Object...arguments) throws SQLException {
		for (int i = 0; i < arguments.length; i++) {
			if ( arguments[i] != null) {
				st.setObject(i + 1, arguments[i]);
			} else {
				st.setNull(i + 1, Types.INTEGER);
			}
		}
	}
	
	private boolean isTableExists(String name) throws SQLException {
		List<Map<String, Object>> res =  sql("select table_name from information_schema.tables where table_name=?", name.toUpperCase());
		return res.size() != 0; 
	}
	
	public H2() throws Exception {
		if (pooledDataSource == null) {
			logger.info("H2 create pool..");
			pooledDataSource = new ComboPooledDataSource();
			pooledDataSource.setDriverClass("org.h2.Driver");
			pooledDataSource.setJdbcUrl(JDBC_URL);
			logger.info("H2 pool ok.");
		}
		
		c = getConnection();
	}

	/**
	 * Возвращает результат выполнения запроса собранный в список с помощью itemsCollector с элементами собранными с помощью itemCollector
	 *
	 * @param sql запрос
	 * @param itemsCollector коллектор списка например Collectors.<Map<String, Object>>toList
	 * @param itemCollector коллектор элемента списка например Collectors.<Pair<String, Object>, String, Object>toMap
	 * @param arguments аргументы запроса
	 * @param <T> тип списка
	 * @param <T1> тип элемента
	 * @return результат
	 * @throws SQLException в случае ошибки выполнения запроса
	 */
	private <T, T1> T sqlInternal(String sql, Collector<T1, ?, T> itemsCollector, Collector<? super Map.Entry<String, Object>, ?, T1> itemCollector, Object...arguments) throws SQLException {
		Collector<T1, T, T> itemsCollectorL = (Collector<T1, T, T>) itemsCollector;
		Collector<Map.Entry<String, Object>, T1, T1> itemCollectorL = (Collector<Map.Entry<String, Object>, T1, T1>)  itemCollector;

		T result = itemsCollectorL.supplier().get();
		try (PreparedStatement st = c.prepareStatement(sql)) {
			fillStatementsParams(st, arguments);

			ResultSet rs = st.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();

			while (rs.next()) {
				T1 item = itemCollectorL.supplier().get();
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					if (rs.getObject(i) != null) {
						itemCollectorL.accumulator().accept(
								item,
								new AbstractMap.SimpleEntry<>(rsmd.getColumnName(i), rs.getObject(i))
						);
					}
					itemsCollectorL.accumulator().accept(result, itemCollectorL.finisher()
							.apply(item));
				}
			}
		}
		return itemsCollectorL.finisher().apply(result);
	}

	private <T> List<T> sqlInternal(String sql, Collector<Map.Entry<String, Object>, ?, T> itemCollector, Object...arguments) throws SQLException {
		return sqlInternal(sql,
				Collectors.toList(),
				itemCollector,
				arguments
		);
	}

	@Override	
	public List<Map<String, Object>> sql(String sql, Object...arguments) throws SQLException {
		List<Map<String, Object>> result = this.sqlInternal(sql,
				Collectors.toList(),
				Collectors.toMap(p -> p.getKey(), p-> p.getValue()),
				arguments
		);
		return result;
	}

	@Override
	public <T> List<T> sqlBeans(String sql, Class<T> beanClass, Object... arguments) throws SQLException {
		return sqlInternal(sql, new BeanCollector<T>(beanClass), arguments);
	}

	@Override
	public <T1, T> T1 sqlBeans(String sql, Collector<T, ?, T1> itemsCollector, Class<? extends T> beanClass, Object... arguments) throws SQLException {
		return sqlInternal(sql, itemsCollector, new BeanCollector<T>(beanClass), arguments);
	}

	@Override
	public int update(String sql, Object...arguments) throws SQLException {
		try (PreparedStatement st = c.prepareStatement(sql)) {
			fillStatementsParams(st, arguments);
			return st.executeUpdate();
		}
	}

	@Override
	public long generateId(String name) throws SQLException {
		String sql = "select nextval('SEQ_" + name.toUpperCase() + "' ) value";
		List<Map<String, Object>> res = sql(sql);		
		return (long) res.get(0).get("VALUE");
	}

	private Object sqlOneValue(String sql, Object...arguments) throws SQLException {
		try (PreparedStatement st = c.prepareStatement(sql)) {
			fillStatementsParams(st, arguments);

			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				return rs.getObject(1);
			} else {
				return null;
			}
		}
	}

	@Override
	public Long getLong(String sql, Object... arguments) throws SQLException {
		return (Long) sqlOneValue(sql, arguments);
	}

	@Override
	public String getString(String sql, Object... arguments) throws SQLException {
		return (String) sqlOneValue(sql, arguments);
	}

	private boolean createTables() throws Exception {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(HENNERY_SQL);
		if (is == null) {
			return false;
		}
		try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
			String versionStr = br.readLine();
			if (!isNewDB(versionStr)) return false;
			ParseScript ps = new ParseScript(br);
			String sql;
			while ((sql = ps.next()) != null) {
				logger.debug("sql = {}", sql);
				update(sql);
			}
			c.commit();
		}
		return true;
	}

	@Override
	public Connection getConnection() throws SQLException {
		if (c != null) return c;
		c = pooledDataSource.getConnection();
		c.setAutoCommit(false);
		return c;
	}

	@Override
	public void free() throws SQLException {
		logger.info("H2 shutting down..");
		if(!c.isClosed()) c.close();
		logger.info("H2 coonnection closed.");
		pooledDataSource.close();
		logger.info("H2 pool closed.");
		logger.info("H2 shutdown complete.");
	}

	@Override
	public void commit() throws SQLException {
		c.commit();		
	}

	@Override
	public void rollback() throws SQLException {
		c.rollback();		
	}
}

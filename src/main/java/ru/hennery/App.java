package ru.hennery;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ServerProperties;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;

import com.pi4j.platform.PlatformManager;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;

import ru.hennery.db.DS;
import ru.hennery.db.H2;
import ru.hennery.elements.alerts.Alert;
import ru.hennery.elements.alerts.DBAlert;
import ru.hennery.elements.scripts.DBScript;
import ru.hennery.elements.scripts.Script;
import ru.hennery.elements.sensors.DBSensor;
import ru.hennery.elements.sensors.Sensor;
import ru.hennery.elements.switches.DBSwitch;
import ru.hennery.elements.switches.Switch;
import ru.hennery.utils.VelocityClasspathResourceLoader;


public class App 
{
	private static final Logger logger = LogManager.getLogger(App.class.getName());
	
	private static AtomicBoolean isShutdown = new AtomicBoolean(false);  
	private static Scheduler sched;
	private static Map<Long, Sensor> sensors;
	private static Map<Long, Switch> switches;
	private static Map<Long, Script> scripts;
	private static Map<Long, Alert> alerts;
	private static Server server;
	private static ScriptEngineManager scriptEngineManager;
	private static ScriptEngine scriptEngine;
	private static RequestLogHandler requestLogHandler; 
		
	public static Server getServer() {
		return server;
	}
	
	public static DS getDS() throws Exception {
		return new H2();
	}
	
	public static Scheduler getSched() {
		return sched;
	}
	
	public static Map<Long, Sensor> getSensors() {
		return sensors;		
	}
	
	public static Map<Long, Switch> getSwitches() {
		return switches;		
	}
	
	public static Map<Long,Script> getScripts() {
		return scripts;
	}
	
	public static Map<Long,Alert> getAlerts() {
		return alerts;
	}
	
	public static ScriptEngine getScriptEngine() {
		return scriptEngine;
	}
	public static RequestLogHandler getRequestLogHandler() {
		return requestLogHandler;
	}
	
	private static void configureVelocity() {
		logger.info("Configure Velocity..");
		Velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		Velocity.setProperty("classpath.resource.loader.class", VelocityClasspathResourceLoader.class.getName());
		
		Velocity.setProperty( RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, org.apache.velocity.runtime.log.Log4JLogChute.class.getName() );
		Velocity.setProperty( "runtime.log.logsystem.log4j.logger", "velocity" );
		Velocity.setProperty("input.encoding", "UTF-8");
		Velocity.setProperty("output.encoding", "UTF-8");
				
		Velocity.init();
		logger.info("Velocity ok.");
	}
	
	private static void configurePI4J() {
		logger.info("Configure PI4J");
		if (PlatformManager.getPlatform() == null) {
			logger.info("platform is null, try once more");
			PlatformManager.getPlatform();
		}
		logger.info("PI4J ok.");
	}
	
	private static void configureH2() throws Exception {
		logger.info("Connect H2..");
		try (DS h2 = new H2()) {
			h2.init();	
		}
		logger.info("H2 ok.");
	}
	
	private static void loadSensors() throws Exception {
		logger.info("Sensors loading ..");
		try (DS ds = getDS()) {
			sensors = ds.sqlBeans("select id from sensor",
					Collectors.<Sensor, Long, Sensor>toMap(Sensor::getId, it -> it),
					DBSensor.class);
		}
		logger.info("Sensors ok. Loaded: {}", sensors.size());
	}
	
	private static void loadSwitches() throws Exception {
		logger.info("Switches loading ..");
		try (DS ds = getDS()) {
			switches = ds.sqlBeans("select id from switch",
					Collectors.<Switch, Long, Switch>toMap(Switch::getId, it -> it),
					DBSwitch.class);
		}
		logger.info("Switches ok. Loaded: {}", switches.size());
	}
	
	private static void syncSwitches() throws Exception {
		logger.info("Switches hardware sync ..");
		
		for(Switch it : switches.values()) it.hardwareSync();
		
		logger.info("Switches hardware sync ok.");
	}

	private static void loadScripts() throws Exception {
		logger.info("Scripts loading ..");
		try (DS ds = getDS()) { 
			scripts = ds.sqlBeans("select id from script",
					Collectors.<Script, Long, Script>toMap(Script::getId, it -> it),
					DBScript.class);
		}
		logger.info("Scripts ok. Loaded: {}", scripts.size() );
	}
	
	private static void loadAlerts() throws Exception {
		logger.info("Alerts loading ..");
		try (DS ds = getDS()) { 
			alerts = ds.sqlBeans("select id from alert",
					Collectors.<Alert, Long, Alert>toMap(Alert::getId, it -> it),
					DBAlert.class);
		}
		logger.info("Alerts ok. Loaded: {}", scripts.size() );
	}
	
	private static void configureQuartz() throws SchedulerException {
		logger.info("Starting Quartz..");
		SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
		sched = schedFact.getScheduler();
		sched.start();
		logger.info("Quartz ok.");
	}
	
	private static void initScriptEngine() {
		logger.info("Init script engine..");
		scriptEngineManager = new ScriptEngineManager();
		scriptEngine = scriptEngineManager.getEngineByName("JavaScript");
		logger.info("Script engine ok.");
	}
	
	private static void configureJetty() throws Exception {
		logger.info("Configure Jetty..");
		
    	server = new Server(80);
		
		HandlerCollection handlers = new HandlerCollection();
		
	   	ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
	   	requestLogHandler = new RequestLogHandler();
	   	requestLogHandler.stop();
	   	
		handlers.setHandlers(new Handler[]{servletContextHandler,requestLogHandler});		
		server.setHandler(handlers);
		
	   	servletContextHandler.setContextPath("/");        
        ServletHolder jerseyServlet = servletContextHandler.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");        
        jerseyServlet.setInitParameter(ServerProperties.PROVIDER_PACKAGES, "ru.hennery.services");        
        jerseyServlet.setInitOrder(0);
		  
		NCSARequestLog requestLog = new NCSARequestLog("jetty-yyyy_mm_dd.request.log");
		requestLog.setRetainDays(90);
		requestLog.setAppend(true);
		requestLog.setExtended(false);
		requestLog.setLogTimeZone("GMT");		
		requestLogHandler.setRequestLog(requestLog);
		
		logger.info("Jetty OK.");
	}
	
	private static void registerShutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				if (!isShutdown.get()) App.logger.info("hennery is interrupted!");
				App.shutdown();
			}
		}));
	}
	
	private static void shutdown() {
		if (!isShutdown.compareAndSet(false, true)) return;
		
		logger.info("hennery is shutting down..");
		try {
			if (sched != null && sched.isStarted()) sched.shutdown(true);
	    	getDS().free();
		} catch (Exception e) {
			logger.catching(e);
		}
		
    	logger.info("hennery stopped");
	}
	
    public static void main( String[] args ) throws Exception
    {
    	logger.info("hennery startup begin");
    	try {
    		
	    	configureVelocity();
	    	configurePI4J();
	    	configureH2();
	    	loadSensors();
	    	loadSwitches();
	    	syncSwitches();
	    	loadScripts();	
	    	loadAlerts();
	    	initScriptEngine();	    	
	    	configureQuartz();
	    	
	    	registerShutdownHook();
	        
	        configureJetty();
	        
	        server.start();	        
	        logger.info("hennery started");
			server.join();
				
    	} catch (Throwable e) {    		
    		logger.catching(e);
    	} finally {
        	shutdown();
        	System.exit(0);
        }
    	
    }

}

package ru.hennery.utils;

import javax.ws.rs.core.MediaType;

public class MediaProduces {
	public static final String PRODUCES_JSON = MediaType.APPLICATION_JSON + ";charset=UTF-8";
}

package ru.hennery.utils;

import java.io.InputStream;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;

public class VelocityClasspathResourceLoader extends ResourceLoader {

	@Override
	public void init(ExtendedProperties configuration) {
		if (log.isTraceEnabled())
        {
            log.trace("ClasspathResourceLoader : initialization complete.");
        }
	}

	@Override
	public InputStream getResourceStream(String source)
			throws ResourceNotFoundException {
		InputStream is = null;
		try {
			is = ClassUtils.getCachedPublicResIS(source);
		} catch (Exception e) {
			throw new ResourceNotFoundException(e);
		}
		return is;
	}

	@Override
	public boolean isSourceModified(Resource resource) {
		return false;
	}

	@Override
	public long getLastModified(Resource resource) {
		return 0;
	}

}

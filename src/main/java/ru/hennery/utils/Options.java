package ru.hennery.utils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;

import ru.hennery.App;
import ru.hennery.db.DS;

public class Options {
	private static final Logger logger = LogManager.getLogger(Options.class.getName());
	
	private static final String MSG_OPT_NOT_FOUND = "Option %s not found!";
	private static final String MSG_GRP_NOT_FOUND = "Group %s not found!";
	
	public static class NotFoundException extends Exception {
		public NotFoundException(String msg) {
			super(msg);
		}
	}
	
	public static Map<String, String> getGroup(String grp) throws Exception {
		try (DS ds = App.getDS()) {
			String sql = "select name, value from options where grp = ?";
			List<Map<String, Object>> res = ds.sql(sql, grp);
			if (res.size() == 0) throw new NotFoundException(String.format(MSG_GRP_NOT_FOUND, grp));
			res.stream()
			.flatMap(map -> map.entrySet().stream())
			.forEach(e -> System.out.println(e.getKey() + " | " + e.getValue()));
			
			Map<String, String> result = res.stream()
				.flatMap(map -> map.entrySet().stream())
				.collect(
						Collectors
							.toMap(e ->  e.getKey().toString(), e -> e.getValue().toString())
			);			
			
			return result;			
		}
	}
	
	public static String getAsString(String name) throws Exception {
		return getAsString(null, name);
	}
	
	public static String getAsString(String grp, String name, String def)  {
		try {
			return getAsString(grp, name);
		} catch (NotFoundException e) {
		} catch (Exception e) {
			logger.catching(e);
		}
		return def;
	}
	
	public static String getAsString(String grp, String name) throws Exception {
		
		try (DS ds = App.getDS()) {
			List<Map<String, Object>> res; 
			if (grp == null) {
				res = ds.sql("select value from options where name = ?", name);	
			} else {
				res = ds.sql("select value from options where grp = ? and name = ?", grp, name);
			}			
			if (res.size() == 0) throw new NotFoundException(String.format(MSG_OPT_NOT_FOUND,name));
			return (String) res.get(0).get("VALUE");
		}

	}
	
	public static void putPUITheme(VelocityContext vc) throws Exception {
		String theme = "ui-lightness";
		try {
			theme = getAsString( "theme");
		} catch (NotFoundException e) {
	
		}
		vc.put("theme", theme);
	}
	
}

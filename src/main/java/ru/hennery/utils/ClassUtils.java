package ru.hennery.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class ClassUtils {
	
	private static final Logger logger = LogManager.getLogger(ClassUtils.class.getName()); 
	
	private static final String RES_CACHE = "Resources";	
	private static final String PUBLIC_DIR = "public/";
	private static final String CN_CACHE = "ClassesInPackage";
	
	public static byte[] getCachedPublicRes(String resName) throws IOException {
		Cache cache = CacheManager.getInstance().getCache(RES_CACHE);
		
		Element el = cache.get(resName);
		if (el == null) {
			InputStream is 
				= Thread.currentThread().getContextClassLoader().getResourceAsStream(PUBLIC_DIR + resName);
			if (is == null) return null;
			
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024 * 16];

			while ((nRead = is.read(data, 0, data.length)) != -1) {
			  buffer.write(data, 0, nRead);
			}

			buffer.flush();
			
			el = new Element(resName, buffer.toByteArray());
			buffer.close();
			cache.put(el);
		}
		
		return (byte[]) el.getObjectValue();
	}
	
	public static InputStream getCachedPublicResIS(String resName) throws IOException {
		byte[] buf = getCachedPublicRes(resName);
		if (buf == null) return null;
		
		ByteArrayInputStream is = new ByteArrayInputStream(buf);
		return is;
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> getCachedClassesNamesInPackage (String packageName) throws Exception {		
		Cache cache = CacheManager.getInstance().getCache(CN_CACHE);
		Element el = cache.get(packageName);
		if (el != null) {
			return ((List<String>) el.getObjectValue());
		}
		
		List<String> classes;
		String scannedPath = packageName.replace('.', '/');
		URL url = Thread.currentThread().getContextClassLoader().getResource(scannedPath);
		String fn = URLDecoder.decode(url.getPath(),"UTF-8");
		if (url.getProtocol().equals("jar")) {
		
			fn = fn.substring(5, fn.indexOf('!'));
			//fn = "c:\\Program Files\\Java\\jre1.8.0_45\\bin\\hennery-0.0.1-SNAPSHOT.jar";
			try(JarFile jf = new JarFile(fn)) {
				classes = jf.stream()
						.filter(it-> !it.isDirectory() && it.getName().endsWith(".class") && it.getName().startsWith(scannedPath))
						.map(f -> f.getName().replaceAll(".class", "").replaceAll(scannedPath + "/", "") )
						.parallel()
						.collect(Collectors.toList());
			}
		} else {
		
		    File dir = new File(fn);	    
			List<File> files = new ArrayList<>(Arrays.asList(dir.listFiles()));
			
			classes = files.stream()
				.filter(f -> (!f.isDirectory() && f.getName().endsWith(".class")))
				.map(f -> f.getName().replaceAll("/", "\\.").replaceAll(".class", ""))
				.collect(Collectors.toList());
			
		}
		if (classes.size() > 0) cache.put(new Element(packageName, classes));
		return classes;
	}

}

/**
 * 
 */
var info = function(msg) {
    $('#msg').puigrowl('show', [{severity: 'info', summary: 'info', detail: msg}]);
};
var warn = function(msg) {
	$('#msg').puigrowl('show', [{severity: 'warn', summary: 'warn', detail: msg}]);
};
var error = function(msg) {
	$('#msg').puigrowl('show', [{severity: 'error', summary: 'error', detail: msg}]);
};

var fmtDateTime = function(dt) {
	if (!dt) return '';
	if (typeof(dt) == 'string') {
		yy = parseInt(dt.substr(0, 4));
		mm = parseInt(dt.substr(5,2)) - 1;
		dd = parseInt(dt.substr(8,2));
		hh = parseInt(dt.substr(11, 2));
		ii = parseInt(dt.substr(14, 2));
		ss = parseInt(dt.substr(17, 2));
		//mils = parseInt(dt.substr(20));
		ldt = new Date(yy, mm, dd, hh, ii, ss);
	} else {
		ldt = dt;
	}
	return $.formatDateTime('dd.mm.yy hh:ii:ss', ldt);
};

var dlgConfirm =
	{				
		
	exec: function(msg, conf) {
		dlgConfirm.conf = conf;
		$('#dlgConfirmMsg').text( msg );
		var dialog = $('#dlgConfirm');
		if (!dlgConfirm.isReady) {
			dialog = $('#dlgConfirm').puidialog({
		    	showEffect: 'fade',  
		        hideEffect: 'fade',  
		        minimizable: false,  
		        maximizable: true,  
		        modal: true,
		        width: 330,
		        buttons: [{  
		                text: 'Да',  
		                icon: 'fa-check',  
		                click:function() {
		                	dlgConfirm.conf(true);
		                	$('#dlgConfirm').puidialog('hide');
		                }
		        
		            },  
		            {  
		                text: 'Нет',  
		                icon: 'fa-close',  
		                click: function() {
		                	dlgConfirm.conf(false);
		                	$('#dlgConfirm').puidialog('hide');
		                }
		            }
		        ]  
		    });
			dlgConfirm.isReady = true;
		}
		dialog.puidialog('show');
	}
}

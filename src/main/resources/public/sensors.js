/**
 * 
 */
Sensors = {
		triggerRowSelect: true, //event propagation не сработало :( этот флаг отключает ее для кнопки вкл/выкл
		
		datasource: function(callback) {
            $.ajax({
                type: "GET",
                url: 'sensors/list',
                dataType: "json",
                context: this,
                success: function(response) {	
                	if (response.stat == 'ERROR') {
                		error(response.msg);
                		return;
                	}
                    callback.call(this, response.result);
                    $('[id^=chk_sens_active]').each(function(i) {                    	
                    	$(this).puitogglebutton(
		        				{   onLabel: 'вкл',
		        			        offLabel: 'выкл',
		        			        onIcon: 'fa-check-square',
		        			        offIcon: 'fa-square',
                        			change: function(event, checked) {
                        				Sensors.triggerRowSelect = false;
                        				var id_sens = $(event.target).attr('id_sens');
                        				$.ajax({
                        					type: 'GET',
                        					url: 'sensors/' + (checked == true ? 'activate' : 'deactivate'),
                        					data: 'id='+ id_sens,
                        					dataType: "json",
                        					context: this,
                        					success: function(response) {	
                        						if (response.stat == 'ERROR') {
                		                    		error(response.msg);
                		                    		$(this).puitogglebutton(checked == true ? 'check' : 'uncheck', true)
                		                    		return;
                		                    	}
                        						$('#btn_sensor_read_' + id_sens).puibutton(checked == true ? 'enable' : 'disable', true);
                        				    }
                        					
                        				});                        				                        				
                        			}
                    			});
                    	
                    });
                }
            });
        },
        btnReadClick: function(event) {
        	$(this).puibutton('disable');
        	var id_sensor = $(this).attr('id_sensor');
        	$.ajax({
				type: 'GET',
				url: 'sensors/read',
				data: 'id=' + id_sensor,
				dataType: "json",
				context: this,
				success: function(response) {	
					if (response.stat == 'ERROR') {
                		error(response.msg);
                		$(this).puibutton('enable');
                		return;                		
                	}
					 $('#txt_sens_result_' + id_sensor).html(Sensors.formatLastReading(response.result));
					$(this).puibutton('enable');					
			    }
        	});
        },
        refreshTable: function() {
        	$('#table_sensors').puidatatable('paginate');
        }, 
        rowSelect: function(event, data) {
        	if (!Sensors.triggerRowSelect) {
        		Sensors.triggerRowSelect = true;
        		return;
        	}
        	Sensors.loadEdtSensor(data);     		
        },
        btnAddClick: function(event) {
        	Sensors.loadEdtSensor(null);
        },
        loadEdtSensor: function(data) {
        	 $('#dlg_edt_sensor').prop('data', data);
        	  $.ajax({
   	 			 type: 'GET',
   	 			 url: 'sensors/drivers',
   	 			 data: 'fmt=labelValue',
   	 			 success: function(response) {
   	 				 if (response.stat == 'ERROR') {
   	             		error(response.msg);
   	             		return;
   	 				 }
   	 				 $('#sel_sdriver').puidropdown({'data': response.labelValue});	
   	 				 if (data == null) {
   	 					$('#edt_sname').val('');
	   	 		        $('#edt_scron').val('');
   	 		        
   	 					$('#dlg_edt_sensor').puidialog('show');
   	 					 return;
   	 				 } 
   	 				 
      	 			 $.ajax({
   	 					 type: 'GET',
   	 					 url: 'sensors/get',
   	 					 data: 'id=' + data.id,
   	 					 success: function(response) {
   	 						 if (response.stat == 'ERROR') {
   	 		             		error(response.msg);
   	 		             		return;
   	 			 			 }
   	 						 $('#edt_sname').val(response.sensor.name);
   	 						 $('#sel_sdriver').puidropdown("selectValue", response.sensor.driverName );
   	 						 $('#edt_scron').val(response.cron);
   	 						 if (response.sensor.lastReading) {
   	 							$.ajax({
   	 			 					 type: 'GET',
   	 			 					 url: 'sensors/readings',
   	 			 					 data: {id: data.id, fmt: 'flot'},
   	 			 					 success: function(response) {
   	 			 						 var d = new Array(response.d.length);
   	 			 						 $(response.d).each(
 			 								 function(i) {
 			 									d[i] = (this.item); 
 			 								 }
   	 			 						 );
   	 			 						 $.plot('#flot_sresults', [d], {
   	 			 							 xaxis: {
		   	 			 						mode: 'time'
		   	 			 						//minTickSize: [1, 'minute']
   	 			 						 		//,
		   	 			 						//min: (new Date(1996, 0, 1)).getTime(),
		   	 			 						//max: (new Date(2000, 0, 1)).getTime()
   	 			 							 }
   	 			 						 });
   	 			 					 }	 							 
   	 							});
   	 						 }
   	 						 $('#dlg_edt_sensor').puidialog('show');			 
   	 					 } 		 
   	 		     	  });
   	 			 }
        	  });
        },
        edtClickOk: function(event) { 
        	var data = $('#dlg_edt_sensor').prop('data');
        	var name = $('#edt_sname').val();
        	var driverName = $('#sel_sdriver').puidropdown('getSelectedValue');
        	var cron = $('#edt_scron').val();
        	name = name == '' ? undefined : name;
        	driverName = driverName = '' ? null : driverName;
        	cron = cron == '' ? undefined : cron;
        	$.ajax({
        		type: 'GET',
        		url: 'sensors/' + (data == null ? 'add' : 'set'),
        		data: {'id': data == null ? undefined : data.id, 'name': name, 'driver_name': driverName, 'cron': cron },
        		success: function(response) {
					 if (response.stat == 'ERROR') {
		             		error(response.msg);
		             		return;
			 		 }
		             $('#dlg_edt_sensor').puidialog('hide');
		             Sensors.refreshTable()  
					 
        		}
        	});
          
        },
        edtClickCancel: function(event) {  
            $('#dlg_edt_sensor').puidialog('hide');  
        }, 
        edtClickDelete: function(event) {
        	var data = $('#dlg_edt_sensor').prop('data');
        	dlgConfirm.exec('Удалить?',
        		function(conf) {
        			if (!conf) return;
        			$.ajax({
        				type: 'GET',
		        		url: 'sensors/del',
		        		data: {'id': data.id},
		        		success: function(response) {
		        			if (response.stat == 'ERROR') {
			             		error(response.msg);
		        			}
		        			$('#dlg_edt_sensor').puidialog('hide');
		        			Sensors.refreshTable();
		        		}
        			});
        		}
        	);
        },
        formatLastReading: function(lastReading) {
        	return lastReading ?
             	  	fmtDateTime(new Date(lastReading.dt)) + '<br/>'
            	  	+ lastReading.value.toFixed(4) + ' ' + lastReading.unit
            	  	: '';
        },
        refresh: function(sensors) {
			 $(sensors).each(
				function(i) {
				 $('#chk_sens_active_' + this.id).puitogglebutton(this.active ? 'check' : 'uncheck');
				 $('#txt_sens_result_' + this.id).html(Sensors.formatLastReading(this.lastReading));
			 	}
			 );
        }
}



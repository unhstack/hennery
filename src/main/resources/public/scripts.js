/**
 * 
 */
Scripts = {
		triggerRowSelect: true, //event propagation не сработало :( этот флаг отключает ее для кнопки вкл/выкл
		dlgEdtCurData: null,  
		datasource: function(callback) {
            $.ajax({
                type: "GET",
                url: 'scripts/list',
                dataType: "json",
                context: this,
                success: function(response) {	
                	if (response.stat == 'ERROR') {
                		error(response.msg);
                		return;
                	}
                    callback.call(this, response.result);
                    $('[id^=chk_script_active]').each(function(i) {
                    	$(this).puitogglebutton(
		        				{   onLabel: 'вкл',
		        			        offLabel: 'выкл',
		        			        onIcon: 'fa-check-square',
		        			        offIcon: 'fa-square',  	
                        			change: function(event, checked) {
                        				Scripts.triggerRowSelect = false;
                        				var id_script = $(event.target).attr('id_script');
                        				$.ajax({
                        					type: 'GET',
                        					url: 'scripts/' + (checked == true ? 'activate' : 'deactivate'),
                        					data: 'id='+ id_script,
                        					dataType: 'json',
                        					context: this,
                        					success: function(response) {
                        						if (response.stat == 'ERROR') {
                		                    		error(response.msg);
                		                    		$(this).puitogglebutton(checked == true ? 'uncheck' : 'check', true);                		                    		
                		                    		return false;
                		                    	}
                        						$('#btn_script_run_' + id_script).puibutton(checked == true? 'enable' : 'disable');
                        				    }
                        				});
                        				
                        			}
                    			});
                    });
                }
            });
        },
        btnRunClick: function(event) {
        	$(this).puibutton('disable');
        	var id_script = $(this).attr('id_script');
        	$.ajax({
				type: 'GET',
				url: 'scripts/run',
				data: 'id=' + id_script,
				dataType: "json",
				context: this,
				success: function(response) {	
					if (response.stat == 'ERROR') {
                		error(response.msg);
                		$(this).puibutton('enable');
                		return;                		
                	}
					info('OK: ' + fmtDateTime(response.script.lastRunningTime));
					$('#div_script_lrt_' + id_script).text(fmtDateTime(response.script.lastRunningTime));
					$(this).puibutton('enable');
			    }
        	});
        },
        refreshTable: function() {
        	$('#table_scripts').puidatatable('paginate');
        }, 
        rowSelect: function(event, data) {
        	if (!Scripts.triggerRowSelect) {
        		Scripts.triggerRowSelect = true;
        		return;
        	}
        	Scripts.loadEdtScript(data);     		
        },
        dsSensors: function(callback) {
        	var data = $('#dlg_edt_script').prop('data');
        	if (!data) {
        		callback.call(this, []);
        		return;
        	}
            $.ajax({
                type: "GET",
                url: 'scripts/list_sensors',
                data: {id: data.id},
                dataType: "json",
                context: this,
                success: function(response) {	
                	if (response.stat == 'ERROR') {
                		error(response.msg);
                		return;
                	}
                    callback.call(this, response.result);
                }
            });
        },
        dsSwitches: function(callback) {
        	var data = $('#dlg_edt_script').prop('data');
        	if (!data) {
        		callback.call(this, []);
        		return;
        	}
        		
        	$.ajax({
                type: "GET",
                url: 'scripts/list_switches',
                data: {id: data.id},
                dataType: "json",
                context: this,
                success: function(response) {	
                	if (response.stat == 'ERROR') {
                		error(response.msg);
                		return;
                	}
                    callback.call(this, response.result);
                }
            });
        },
        btnAddClick: function(event) {
        	Scripts.loadEdtScript(null);
        },
        loadEdtScript: function(data) {
			$('#dlg_edt_script').prop('data', data);
			var load = 3;
			if (data == null) {
				$('#edt_mname').val('');
				$('#edt_mcron').val('');
				$('#edt_script').val('');
				
				load = 2;				
			} else {
				$.ajax({
					 type: 'GET',
					 url: 'scripts/get',
					 data: 'id=' + data.id,
					 success: function(response) {
						 if (response.stat == 'ERROR') {
					 		error(response.msg);
					 		return;
						 }
						 $('#edt_mname').val(response.script.name);					 
						 $('#edt_mcron').val(response.cron);
						 $('#edt_mscriptbody').val(response.script.scriptBody);
						 
						 if (--load == 0) $('#dlg_edt_script').puidialog('show');			 
					 } 		 
				 });


			}
			
     		$.ajax({
				 type: 'GET',
				 url: 'sensors/list',
				 data: 'fmt=labelValue',
				 success: function(response) {
					 if (response.stat == 'ERROR') {
				 		error(response.msg);
				 		return;
					 }
					 $('#sel_msensors').puidropdown({'data': response.labelValue});	
					 					 
					 if (--load == 0) $('#dlg_edt_script').puidialog('show');			 
				 } 		 
			 });
			 
			 $.ajax({
				 type: 'GET',
				 url: 'switches/list',
				 data: 'fmt=labelValue',
				 success: function(response) {
					 if (response.stat == 'ERROR') {
				 		error(response.msg);
				 		return;
					 }
					 $('#sel_mswitches').puidropdown({'data': response.labelValue});	
					 					 
					 if (--load == 0) $('#dlg_edt_script').puidialog('show');			 
				 } 		 
			 });
			 
			 $('#table_msensors').puidatatable('paginate');
			 $('#table_mswitches').puidatatable('paginate');
        },
        sensorsRowSelect: function(event, data) {
        	$('#sel_msensors').puidropdown("selectValue", data.idSensor);
        	$('#edt_msensscriptname').val(data.scriptName);
        },
        switchesRowSelect: function(event, data) {
        	$('#sel_mswitches').puidropdown("selectValue", data.idSwitch);
        	$('#edt_mswscriptname').val(data.scriptName);        	
        },
        edtClickAddSensor: function(event) {
        	var idSensor   = $('#sel_msensors').val();
        	var data 	   = $('#dlg_edt_script').prop('data');
        	var scriptName = $("#edt_msensscriptname").val();
        	
        	$.ajax({
				 type: 'GET',
				 url: 'scripts/add_sensor',
				 data: {id: data.id, id_sensor: idSensor, script_name: scriptName },
				 success: function(response) {
					 if (response.stat == 'ERROR') {
					 		error(response.msg);
					 		return;
					 }
					 $('#table_msensors').puidatatable('paginate');
				 }
        	});
        },
        edtClickDelSensor: function(event) {
        	var idSensor   = $('#sel_msensors').val();
        	var data 	   = $('#dlg_edt_script').prop('data');
        	$.ajax({
				 type: 'GET',
				 url: 'scripts/remove_sensor',
				 data: {id: data.id, id_sensor: idSensor},
				 success: function(response) {
					 if (response.stat == 'ERROR') {
					 		error(response.msg);
					 		return;
					 }
					 $('#table_msensors').puidatatable('paginate');
				 }
        	});
        },
        edtClickAddSwitch: function(event) {
        	var idSwitch   = $('#sel_mswitches').val();
        	var data 	   = $('#dlg_edt_script').prop('data');
        	var scriptName = $('#edt_mswscriptname').val();
        	
        	$.ajax({
				 type: 'GET',
				 url: 'scripts/add_switch',
				 data: {id: data.id, id_switch: idSwitch, script_name: scriptName },
				 success: function(response) {
					 if (response.stat == 'ERROR') {
					 		error(response.msg);
					 		return;
					 }
					 $('#table_mswitches').puidatatable('paginate');
				 }
        	});
        },
        edtClickDelSwitch: function(event) {
        	var idSwitch   = $('#sel_mswitches').val();
        	var data 	   = $('#dlg_edt_script').prop('data');
        	$.ajax({
				 type: 'GET',
				 url: 'scripts/remove_switch',
				 data: {id: data.id, id_switch: idSwitch},
				 success: function(response) {
					 if (response.stat == 'ERROR') {
					 		error(response.msg);
					 		return;
					 }
					 $('#table_mswitches').puidatatable('paginate');
				 }
        	});
        },
        edtClickOk: function(event) { 
        	var data = $('#dlg_edt_script').prop('data');
        	var name = $('#edt_mname').val();        	
        	var cron = $('#edt_mcron').val();
        	var scriptBody = $('#edt_mscriptbody').val();
        	
        	name = name == '' ? undefined : name;
        	cron = cron == '' ? undefined : cron;
        	
        	$.ajax({
        		type: 'GET',
        		url: 'scripts/' + (data == null ? 'add' : 'set'),
        		data: {'id': data == null ? undefined : data.id, 'name': name, 'cron': cron, 'scriptBody': scriptBody },
        		success: function(response) {
					 if (response.stat == 'ERROR') {
		             		error(response.msg);
		             		return;
			 		 }
					 $('#dlg_edt_script').puidialog('hide');
			         Scripts.refreshTable();
        		}
        	});
        },
        edtClickCancel: function(event) {  
            $('#dlg_edt_script').puidialog('hide');  
        }, 
        edtClickDelete: function(event) {
        	var data = $('#dlg_edt_script').prop('data');
        	dlgConfirm('Удалить?',
        		function(conf) {
        			if (!conf) return;
        			$.ajax({
        				type: 'GET',
		        		url: 'scripts/del',
		        		data: {'id': data.id},
		        		success: function(response) {
		        			if (response.stat == 'ERROR') {
			             		error(response.msg);
		        			}
		        			$('#dlg_edt_script').puidialog('hide'); 
		        			Scripts.refreshTable();
		        		}
        			});
        		}
        	);
        	
        },
        refresh: function(scripts) {
			 $(scripts).each(
				function(i) {
				 $('#chk_script_active_' + this.id).puitogglebutton(this.active ? 'check' : 'uncheck');
			 	}
			 );
       }
}
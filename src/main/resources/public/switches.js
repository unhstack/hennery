/**
 * 
 */
Switches = {
		triggerRowSelect: true, //event propagation не сработало :( этот флаг отключает ее для кнопки вкл/выкл
		
		datasource: function(callback) {
            $.ajax({
                type: "GET",
                url: 'switches/list',
                dataType: "json",
                context: this,
                success: function(response) {	
                	if (response.stat == 'ERROR') {
                		error(response.msg);
                		return;
                	}
                    callback.call(this, response.result);
                    
                    $('[id^=chk_switch_active]').each(function(i) {                    	
                    	$(this).puitogglebutton(
		        				{   onLabel: 'вкл',
		        			        offLabel: 'выкл',
		        			        onIcon: 'fa-check-square',
		        			        offIcon: 'fa-square',  	
                        			change: function(event, checked) {
                        				Switches.triggerRowSelect = false;
                        				var id_switch = $(event.target).attr('id_switch');
                        				$.ajax({
                        					type: 'GET',
                        					url: 'switches/' + (checked == true ? 'activate' : 'deactivate'),
                        					data: 'id='+ id_switch,
                        					dataType: "json",
                        					context: this,
                        					success: function(response) {	
                        						if (response.stat == 'ERROR') {
                		                    		error(response.msg);
                		                    		$(this).puitogglebutton(checked == true ? 'uncheck' : 'check')                		                    		
                		                    		return false;
                		                    	}
                        						$('#chk_switch_status_' + id_switch).puitogglebutton(checked == true ? 'enable' : 'disable');
                        				    }
                        				});
                        				
                        			}
                    			});
                    	
                    });
                    
                    $('[id^=chk_switch_status]').each(function(i) {
                    	$(this).puitogglebutton(
		        				{   onLabel: 'вкл',
		        			        offLabel: 'выкл',
		        			        onIcon: 'fa-check-square',
		        			        offIcon: 'fa-square',  	
                        			change: function(event, checked) {
                        				Switches.triggerRowSelect = false;
                        				var id_switch = $(event.target).attr('id_switch');
                        				$.ajax({
                        					type: 'GET',
                        					url: 'switches/' + (checked == true ? 'on' : 'off'),
                        					data: 'id='+ id_switch,
                        					dataType: "json",
                        					context: this,
                        					success: function(response) {	
                        						if (response.stat == 'ERROR') {
                		                    		error(response.msg);
                		                    		$(this).puitogglebutton(checked == true ? 'uncheck' : 'check', true)
                		                    		return;
                		                    	}
                        						$('#div_switch_st_' + id_switch).text(fmtDateTime(response.sw.lastStatusChange));
                        				    }
                        				});
                        				
                        			}
                    			});
                    });
                    
                }
            });
        },
        refreshTable: function() {
        	 $('#table_switches').puidatatable('paginate');
        }, 
        rowSelect: function(event, data) {
        	if (!Switches.triggerRowSelect) {
        		Switches.triggerRowSelect = true;
        		return;
        	}
        	Switches.loadEdtSwitch(data);     		
        },
        btnAddClick: function(event) {
        	Switches.loadEdtSwitch(null);
        },
        loadEdtSwitch: function(data) {
        	
			$('#dlg_edt_switch').prop('data', data);
			  $.ajax({
				 type: 'GET',
				 url: 'switches/drivers',
				 data: 'fmt=labelValue',
				 success: function(response) {
					 if (response.stat == 'ERROR') {
			     		error(response.msg);
			     		return;
					 }
					 $('#sel_swdriver').puidropdown({'data': response.labelValue});	
					 if (data == null) {
						$('#edt_swname').val('');
				        $('#edt_swcronon').val('');
				        $('#edt_swcronoff').val('');
						$('#dlg_edt_switch').puidialog('show');
						 return;
					 }
					 
					 $.ajax({
						 type: 'GET',
						 url: 'switches/get',
						 data: 'id=' + data.id,
						 success: function(response) {
							 if (response.stat == 'ERROR') {
			             		error(response.msg);
			             		return;
				 			 }
							 $('#edt_swname').val(response.sw.name);
							 $('#sel_swdriver').puidropdown("selectValue", response.sw.driverName );
							 $('#edt_swcronon').val(response.cronOn);
							 $('#edt_swcronoff').val(response.cronOff);							 
							 $('#dlg_edt_switch').puidialog('show');			 
						 } 		 
			     	  });   	 		     	  	 				 
				 }
			});
        },
        edtClickOk: function(event) { 
        	var data = $('#dlg_edt_switch').prop('data');
        	var name = $('#edt_swname').val();
        	var driverName = $('#sel_swdriver').puidropdown('getSelectedValue');
        	var cronOn = $('#edt_swcronon').val();
        	var cronOff = $('#edt_swcronoff').val();
        	name = name == '' ? undefined : name;
        	driverName = driverName = '' ? null : driverName;
        	cronOn = cronOn == '' ? undefined : cronOn;
        	cronOff = cronOff == '' ? undefined : cronOff;
        	$.ajax({
        		type: 'GET',
        		url: 'switches/' + (data == null ? 'add' : 'set'),
        		data: {'id': data == null ? undefined : data.id, 'name': name, 'driver_name': driverName, 'cron_on': cronOn, 'cron_off': cronOff },
        		success: function(response) {
					 if (response.stat == 'ERROR') {
		             		error(response.msg);
		             		return;
			 		 }
					 Switches.refreshTable();
					 $('#dlg_edt_switch').puidialog('hide');
        		}
        	});
             
        },
        edtClickCancel: function(event) {  
            $('#dlg_edt_switch').puidialog('hide');  
        }, 
        edtClickDelete: function(event) {
        	var data = $('#dlg_edt_switch').prop('data');
        	dlgConfirm.exec('Удалить?',
        		function(conf) {
        			if (!conf) return;
        			$.ajax({
        				type: 'GET',
		        		url: 'switches/del',
		        		data: {'id': data.id},
		        		success: function(response) {
		        			if (response.stat == 'ERROR') {
			             		error(response.msg);
		        			}
		        			$('#dlg_edt_switch').puidialog('hide'); 
		        			Switches.refreshTable();
		        		}
        			});
        		}
        		
        	);
        },
        refresh: function(switches) {
			 $(switches).each(
				function(i) {
				 $('#chk_switch_active_' + this.id).puitogglebutton(this.active ? 'check' : 'uncheck');
				 $('#chk_switch_status_' + this.id).puitogglebutton(this.status ? 'check' : 'uncheck');
				 $('#div_switch_st_' + this.id).text(fmtDateTime(this.lastStatusChange));
			 	}
			 );
       }
}